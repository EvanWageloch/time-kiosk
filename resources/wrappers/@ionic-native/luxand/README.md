<a style="float:right;font-size:12px;" href="http://github.com/ionic-team/ionic-native/edit/master/src/@ionic-native/plugins/luxand/index.ts#L28">
  Improve this doc
</a>

# Luxand

```
$ ionic cordova plugin add codova-plugin-luxand
$ npm install @ionic-native/luxand
```

## [Usage Documentation](https://ionicframework.com/docs/native/luxand/)

Plugin Repo: [https://github.com/molobala/cordova-plugin-luxand](https://github.com/molobala/cordova-plugin-luxand)

This plugin let you integrat Luxand Face SDK into your ionic projects, so you can implements face authentication easily in your applications.

## Supported platforms

- Android
  - iOS
  


