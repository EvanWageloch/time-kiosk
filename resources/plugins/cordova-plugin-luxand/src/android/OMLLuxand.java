package com.luxand.dsi;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;

import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;

import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.luxand.FSDK;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Date;

import au.com.wageloch.timekiosk.R;

public class OMLLuxand extends Activity implements OnClickListener {

    private boolean mIsFailed = false;
    private Preview mPreview;
    private ProcessImageAndDrawResults mDraw;
    private  String database = "Memory50.dat";
    private int loginTryCount = 3;
    private int timeOut;
    private String name;
    private boolean darkMode;
    private String launchType = "FOR_REGISTER";
    private final String help_text = "Luxand Face Recognition\n\nJust tap any detected face and name it. The app will recognize this face further. For best results, hold the device at arm's length. You may slowly rotate the head for the app to memorize you at multiple views. The app can memorize several persons. If a face is not recognized, tap and name it again.\n\nThe SDK is available for mobile developers: www.luxand.com/facesdk";


    public void showErrorAndClose(String error, int code) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(error + ": " + code)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                })
                .show();
    }

    public void showMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .setCancelable(false) // cancel with button only
                .show();
    }

    private void resetTrackerParameters() {
        int errpos[] = new int[1];
        FSDK.SetTrackerMultipleParameters(mDraw.mTracker, "DetectFacialFeatures=true;ContinuousVideoFeed=true;FacialFeatureJitterSuppression=0;RecognitionPrecision=1;Threshold=0.996;Threshold2=0.9995;ThresholdFeed=0.97;MemoryLimit=2000;HandleArbitraryRotations=false;DetermineFaceRotationAngle=true;InternalResizeWidth=70;FaceDetectionThreshold=3;", errpos);
        if (errpos[0] != 0) {
            showErrorAndClose("Error setting tracker parameters, position", errpos[0]);
        }
        FSDK.SetTrackerMultipleParameters(mDraw.mTracker, "DetectAge=true;DetectGender=true;DetectExpression=true", errpos);
        if (errpos[0] != 0) {
            showErrorAndClose("Error setting tracker parameters 2, position", errpos[0]);
        }

        FSDK.SetTrackerParameter(mDraw.mTracker, "KeepFaceImages", "false");

        // faster smile detection
        FSDK.SetTrackerMultipleParameters(mDraw.mTracker, "AttributeExpressionSmileSmoothingSpatial=0.5;AttributeExpressionSmileSmoothingTemporal=10;", errpos);
        if (errpos[0] != 0) {
            showErrorAndClose("Error setting tracker parameters 3, position", errpos[0]);
        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hide the window title (it is done in manifest too)
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY );

        // Lock orientation
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
//        setContentView(R.layout.main);

        Constants.sDensity = getResources().getDisplayMetrics().scaledDensity;

        int res;
        // = FSDK.ActivateLibrary("");
        // if (res != FSDK.FSDKE_OK) {
        //     mIsFailed = true;
        //     showErrorAndClose("FaceSDK activation failed", res);
        // } else {
        //     FSDK.Initialize();

            

        // }

        // Camera layer and drawing layer
        Bundle data = getIntent().getExtras();
        if(data != null) {
            this.database = data.getString("DB_NAME", "memory.dat");
            Log.e("com.luxand.dsi", database);
            this.loginTryCount = data.getInt("LOGIN_TRY_COUNT", 3);
            this.launchType = data.getString("TYPE", "FOR_REGISTER");
            this.timeOut = data.getInt("TIMEOUT", 15000);
            this.name = data.getString("NAME", "WL-LUXAND"+new Date().getTime());
            this.darkMode = data.getBoolean("DARKMODE", false);
        }
        mDraw = new ProcessImageAndDrawResults(this, this.launchType.equals("FOR_REGISTER"), loginTryCount, timeOut, name);
        mDraw.setOnImageProcessListener(new OnImageProcessListener() {
            @Override
            public void handle(JSONObject obj) {
                Intent data = new Intent();
                if(obj==null) obj = new JSONObject();
                data.putExtra("data", obj.toString());
                setResult(RESULT_OK, data);
                _pause();
                finish();
            }
        });
        mPreview = new Preview(this, mDraw);
        mDraw.mTracker = new FSDK.HTracker();

        String templatePath = this.getApplicationInfo().dataDir + "/" + database;
        if (FSDK.FSDKE_OK != FSDK.LoadTrackerMemoryFromFile(mDraw.mTracker, templatePath)) {
            res = FSDK.CreateTracker(mDraw.mTracker);
            if (FSDK.FSDKE_OK != res) {
                showErrorAndClose("Error creating tracker", res);
            }
        }

        setContentView(R.layout.main);

        SurfaceView surfaceView = findViewById(R.id.surfaceView);
        surfaceView.getHolder().addCallback(mPreview);

        addContentView(mDraw, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        resetTrackerParameters(); //black background

        setCustomBranding();

        lockOrientation(this);

        // Menu
        findViewById(R.id.registerButton).setOnClickListener(this);
        findViewById(R.id.passwordButton).setOnClickListener(this);
        findViewById(R.id.cancelButton).setOnClickListener(this);
    }

    public void setCustomBranding() {
        Typeface montserrat_bold = Typeface.createFromAsset(getAssets(),
                "font/montserrat_bold.ttf");
        Typeface montserrat = Typeface.createFromAsset(getAssets(),
                "font/montserrat.ttf");

        ((TextView)findViewById(R.id.textView)).setTypeface(montserrat_bold);
        ((Button)findViewById(R.id.registerButton)).setTypeface(montserrat);
        ((Button)findViewById(R.id.passwordButton)).setTypeface(montserrat);
        ((Button)findViewById(R.id.cancelButton)).setTypeface(montserrat);

        if (this.darkMode) {
            ((ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.mbackground_dark);
        } else {
            ((ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.mbackground);
        }

        ((ImageView)findViewById(R.id.imageView2)).setImageResource(R.drawable.wageloch_logo);
    }

    public static void lockOrientation(Activity activity) {
        Display display = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        int tempOrientation = activity.getResources().getConfiguration().orientation;
        int orientation = 0;
        switch(tempOrientation)
        {
            case Configuration.ORIENTATION_LANDSCAPE:
                if(rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90)
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                else
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                if(rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_270)
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                else
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
        }
        activity.setRequestedOrientation(orientation);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.registerButton) {
            showLoading();
            JSONObject obj = new JSONObject();
            try {
                obj.put("status", "REGISTER");
                obj.put("error", false);
                obj.put("customError", true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent data = new Intent();
            data.putExtra("data", obj.toString());
            setResult(RESULT_OK, data);
            finish();
        } else if (view.getId() == R.id.passwordButton) {
            showLoading();
            JSONObject obj = new JSONObject();
            try {
                obj.put("status", "PASSWORD");
                obj.put("error", false);
                obj.put("customError", true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent data = new Intent();
            data.putExtra("data", obj.toString());
            setResult(RESULT_OK, data);
            finish();
        } else if (view.getId() == R.id.cancelButton) {
            showLoading();
            JSONObject obj = new JSONObject();
            try {
                obj.put("status", "CANCEL");
                obj.put("error", false);
                obj.put("customError", true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent data = new Intent();
            data.putExtra("data", obj.toString());
            setResult(RESULT_OK, data);
            finish();
        }
//        if (view.getId() == getAppResource("helpButton", "id")) {
//            showMessage(help_text);
//        } else if (view.getId() == getAppResource("clearButton", "id")) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setMessage("Are you sure to clear the memory?" )
//                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                        @Override public void onClick(DialogInterface dialogInterface, int j) {
//                            pauseProcessingFrames();
//                            FSDK.ClearTracker(mDraw.mTracker);
//                            resetTrackerParameters();
//                            resumeProcessingFrames();
//                        }
//                    })
//                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                        @Override public void onClick(DialogInterface dialogInterface, int j) {
//                        }
//                    })
//                    .setCancelable(false) // cancel with button only
//                    .show();
//        }
    }

    public void showLoading() {
        ImageView imageView = findViewById(R.id.imageView3);
        imageView.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        if (this.darkMode) {
            Glide.with(this).load(R.drawable.loading_dark).into(imageView);
        } else {
            Glide.with(this).load(R.drawable.loading).into(imageView);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        _pause();
    }
    private void _pause() {
        pauseProcessingFrames();
        String templatePath = this.getApplicationInfo().dataDir + "/" + database;
        FSDK.SaveTrackerMemoryToFile(mDraw.mTracker, templatePath);
    }
    @Override
    public void onResume() {
        super.onResume();
        if (mIsFailed)
            return;
        resumeProcessingFrames();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        //Intent intent = new Intent("xper.activity.ACTIVITY_BAR_RESULT_INTENT");
        //intent.putExtra("codBar", "bar");
        //setResult(Activity.RESULT_CANCELED, intent);
        setResult(RESULT_CANCELED);
        _pause();
        finish();
    }

    private void pauseProcessingFrames() {
        mDraw.mStopping = 1;

        // It is essential to limit wait time, because mStopped will not be set to 0, if no frames are feeded to mDraw
        for (int i=0; i<100; ++i) {
            if (mDraw.mStopped != 0) break;
            try { Thread.sleep(10); }
            catch (Exception ex) {}
        }
    }

    private void resumeProcessingFrames() {
        mDraw.mStopped = 0;
        mDraw.mStopping = 0;
    }
    private int getAppResource(String name, String type) {
        return getResources().getIdentifier(name, type, getPackageName());
    }
}




