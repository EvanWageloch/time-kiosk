export const environment = {
  production: true,
  ksec: 'CAvrsb2352rdgh32feo53ijh1Dwefs81',
  apiHost: 'https://clocktest.wageloch.com.au/',
  luxandLicense: 'jnpX2XMskwMWM/UhCu1gg/drl1gyecxWEPl34EaNFXesk8t3Ld2PPBDA4I41dkuyAWKduNYylcGiJ/RFN7xam6cR/NdQWsalpKmPt81QoLtNMvVvjGNSJv1fjj4TagNehjJ9xdkNaPcRxCw7JCm3vUTZXAuW7C+zyo9GyV875s0='
};
