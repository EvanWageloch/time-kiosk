// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  ksec: 'CAvrsb2352rdgh32feo53ijh1Dwefs81',
  apiHost: 'https://clocktest.wageloch.com.au/',
  luxandLicense: 'jnpX2XMskwMWM/UhCu1gg/drl1gyecxWEPl34EaNFXesk8t3Ld2PPBDA4I41dkuyAWKduNYylcGiJ/RFN7xam6cR/NdQWsalpKmPt81QoLtNMvVvjGNSJv1fjj4TagNehjJ9xdkNaPcRxCw7JCm3vUTZXAuW7C+zyo9GyV875s0='
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
