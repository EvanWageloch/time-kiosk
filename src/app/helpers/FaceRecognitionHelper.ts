import { Injectable } from "@angular/core";
import { Device } from '@ionic-native/device/ngx';
import { File } from '@ionic-native/file/ngx';
import { Luxand } from 'resources/wrappers/@ionic-native/luxand/ngx';
import { environment } from 'src/environments/environment';
import { UserPreferencesHelper } from './UserPreferencesHelper';

@Injectable()
export class FaceRecognitionHelper {

    dbLogs: string = '';
    dbName = 'facedb.dat';

    registeredFaces = {};

    constructor(private luxand: Luxand, private file: File, private userPreferencesHelper: UserPreferencesHelper, private device: Device) {

    }

    async initialise(): Promise<void> {
        if (!this.device.cordova) return;
        await this.luxand.init({
            dbname: this.dbName,
            loginTryCount: 5,
            licence: environment.luxandLicense
        });
        this.registeredFaces = await this.userPreferencesHelper.get(UserPreferencesHelper.REGISTERED_FACES);
        if (!this.registeredFaces) this.registeredFaces = {};
    }

    checkDB() {
        this.file.checkFile(this.file.applicationStorageDirectory, "Documents/" + this.dbName).then(val => {
            console.log("FILE Exists in data dir: " + this.file.applicationStorageDirectory + ":" + JSON.stringify(val));
        }).catch(err => {
            console.log("File not found: " + this.file.applicationStorageDirectory + ":" + JSON.stringify(err));
        });

        this.file.resolveDirectoryUrl(this.file.applicationStorageDirectory).then(val => {
            val.getFile("Documents/" + this.dbName, null, (entry) => {
                console.log("FileName: ", entry.name);
                entry.file((file => {
                    this.dbLogs += "File Size: " + file.size + "\n\n";
                    console.log("File Size: " + file.size);
                }));
            });
        });
    }

    register(name: string, darkMode: string) {
        return this.luxand.register({ timeout: -1, name: name, darkMode: darkMode });
    }

    login(darkMode: string) {
        return this.luxand.login({ timeout: -1, darkMode: darkMode });
    }

    clear(name) {
        console.log(this.registeredFaces[name]);
        this.luxand.clear(this.registeredFaces[name]).then(val => {
            console.log(val);
            delete this.registeredFaces[name];
            this.userPreferencesHelper.set(UserPreferencesHelper.REGISTERED_FACES, this.registeredFaces);
        }).catch(err => {
            delete this.registeredFaces[name];
            this.userPreferencesHelper.set(UserPreferencesHelper.REGISTERED_FACES, this.registeredFaces);
            console.log("Error when deleting face: " + JSON.stringify(err));
        });
    }

    clearAll() {
        this.registeredFaces = {};
        return this.luxand.clearMemory();
    }

    saveRegisteredFace(name: string, id) {
        this.registeredFaces[name] = id;
        this.userPreferencesHelper.set(UserPreferencesHelper.REGISTERED_FACES, this.registeredFaces);
    }

    isRegistered(name: string) {
        return Object.keys(this.registeredFaces).includes(name);
    }

}