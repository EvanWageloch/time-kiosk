import { Injectable } from '@angular/core';

@Injectable()
export class ConstantsHelper {
    public static ClockMethods = [
        {
            text: 'No clocking from this device',
            value: 0
        },
        {
            text: 'Password (min 5 chars)',
            value: 1
        },
        {
            text: 'Pin Code (4 digits)',
            value: 100
        },
        {
            text: 'Click on name only to clock in/out',
            value: 200
        },
        {
            text: 'Facial Recognition with name selection',
            value: 300
        },
        {
            text: 'Facial Recognition with no name selection',
            value: 400
        },
        {
            text: 'Facial Recognition (Touchless)',
            value: 500
        }
    ];
}