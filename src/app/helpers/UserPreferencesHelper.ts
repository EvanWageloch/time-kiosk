import { Injectable } from "@angular/core";
import { Platform } from '@ionic/angular';
import { AlertHelper } from './AlertHelper';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';

declare var require: any;

@Injectable()
/**
 * This class handles both the local storage and the secured local storage
 * Contains constants of all the keys that are being saved to the storage
 */
export class UserPreferencesHelper {

    CryptoJS = require('crypto-js');

    public static LOGGED_IN = 'logged_in'; //logged in or not
    public static CLOCK_CONFIG = 'clock_config';
    public static CONFIG_STATE = 'config_state';
    public static TOKENS = 'tokens';
    public static STAFF_LIST = 'staff_list';
    public static REGISTERED_FACES = 'registered_faces';

    public static SELECTED_SITE_INDEX = 'selected_site_index'; // Index of currently selected site
    public static DEFAULT_DEPTSANDROLES = 'default_deptsandroles'; // Default departments and roles for sites=
    public static PENDING_LOGS = 'pending_logs'; // Array of pending clock in/outs
    public static PENDING_NOTES = 'pending_notes'; // Array of pending notes

    constructor(private normalStorage: Storage, private alertHelper: AlertHelper) {
    }

    public async createStorage(): Promise<any> {
        return this.normalStorage.ready();
    }

    /**
     * Saves a value to the storage
     * @param key Key to save to
     * @param val Value to save
     */
    public set(key: string, val: any): Promise<any> {
        // Encrypt
        var ciphertext = this.CryptoJS.AES.encrypt(JSON.stringify(val), environment.ksec);
        
        // Convert to JSON and save
        return this.normalStorage.set(key, JSON.parse('{"data": "' + ciphertext +'"}'));
    }

    public get(key: string): Promise<any> {
        return new Promise((res, err) => {
            this.normalStorage.get(key).then(val => {
                // Decrypt
                var data = this.CryptoJS.AES.decrypt(val.data, environment.ksec).toString(this.CryptoJS.enc.Utf8);
                // Convert to JSON
                try {
                    res(JSON.parse(data));
                } catch(error) {
                    res(data);
                }
            }).catch(error1 => {
                res(null);
            });
        });
    }

    public remove(key: string) {
        this.normalStorage.remove(key);
    }

    /**
     * Clears everything
     */
    public clearPrefs() {
        this.normalStorage.clear();
    }

}