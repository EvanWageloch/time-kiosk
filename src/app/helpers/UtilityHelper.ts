import { Injectable } from "@angular/core";

@Injectable()
export class UtilityHelper {

    dates: number[] = [];
    months: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    years: number[] = [new Date().getFullYear(), new Date().getFullYear() + 1, new Date().getFullYear() + 2];

    constructor() {
        this.populateDays(new Date().getFullYear(), new Date().getMonth());
    }

    convertMonthToString(month: number, full: boolean = true) {
        const monthNames = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        ];
        const monthNamesFull = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ];
        return full ? monthNamesFull[month] : monthNames[month];
    }

    convertDayToString(day: number, full: boolean) {
        const daysFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        return full ? daysFull[day] : days[day];
    }

    convertDateToString(year: number, month: number, date: number) {
        return new Date(year, month, date).toLocaleDateString('en-AU', { weekday: 'long' });
    }

    getOrdinal(d: number) {
        if (d > 3 && d < 21) return 'th';
        switch (d % 10) {
            case 1: return "st";
            case 2: return "nd";
            case 3: return "rd";
            default: return "th";
        }
    }

    formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    formatAMPM_ClockTime(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'p' : 'a';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        var hoursString = hours < 10 ? '0' + hours : hours;
        var minuteString = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hoursString + ':' + minuteString + ' ' + ampm;
        return strTime;
    }

    populateDays(year: number, month: number) {
        this.dates = [];
        var days = new Date(year, month + 1, 0).getDate();
        for (var i = 1; i <= days; i++) {
            this.dates.push(i);
        }
    }

    formattedTime(d) {
        var localTime = new Date(d);
        var year = localTime.getFullYear();
        var month = localTime.getMonth() + 1;
        var date = localTime.getDate();
        var hours = localTime.getHours();
        var minutes = localTime.getMinutes();
        var seconds = localTime.getSeconds();
        var datetime = year + "/" + month + "/" + date + " " + hours + ":" + minutes + ":" + seconds;
        return datetime;
    }

    parseFormattedTime(d: string, separator: '-' | '/') {
        var parts = d.split(separator);
        var year = parseInt(parts[0]);
        var month = parseInt(parts[1]) - 1;

        var parts1 = parts[2].split(' ');
        var date = parseInt(parts1[0]);

        var parts2 = parts1[1].split(':');
        var hours = parseInt(parts2[0]);
        var minutes = parseInt(parts2[1]);

        return new Date(year, month, date, hours, minutes);
    }

    formattedDate(localTime) {
        var year = localTime.getFullYear();
        var month = ('0' + (localTime.getMonth() + 1)).substr(-2);
        var date = ('0' + localTime.getDate()).substr(-2);
        return year + "/" + month + "/" + date;
    }

    dateWithHiphen(d) {
        var localTime = new Date(d);
        var year = localTime.getFullYear();
        var month = ('0' + (localTime.getMonth() + 1)).substr(-2);
        var date = ('0' + localTime.getDate()).substr(-2);
        return year + "-" + month + "-" + date;
    }

    isPastDate(year: number, month: number, date: number) {
        var now = new Date();
        now.setHours(0, 0, 0, 0);
        return new Date(year, month, date) < now;
    }

    capitaliseString(s: string) {
        return s.replace(/(?:^|\s)\S/g, function (a) { return a.toUpperCase(); });
    }

    convertTimestampToDate(timestamp) {
        var date = new Date(timestamp);
        return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
    }

    isValidUrl(string) {
        return (/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/).test(string);
    }

    dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, { type: mime });
    }

}