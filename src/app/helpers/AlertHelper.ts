import { Injectable } from "@angular/core";
import { AlertController } from '@ionic/angular';

@Injectable()
/**
 * Helper class for creating custom styled alerts.
 * Contains functions for creating, presenting with and without dismiss buttons.
 */
export class AlertHelper {

    alert: HTMLIonAlertElement;

    constructor(private alertController: AlertController) {

    }

    /**
     * Presents an alert that contains two buttons
     * @param msg Message to contain in the alert view
     * @param okButtonText Text for the positive button
     * @param dissmissButtonText Text for the negative button
     * @param dismissable True if the user can tap on the background to dismiss the alert
     * @param okButtonHandler Function to call when positive button is pressed
     * @param dissmissButtonHandler Function to call when negative button is pressed
     */
    async present(msg: string, okButtonText: string, dissmissButtonText: string, dismissable: boolean, okButtonHandler: () => void, dissmissButtonHandler: () => void) {
        this.alert = await this.alertController.create({
            backdropDismiss: dismissable,
            message: msg,
            cssClass: 'custom-alert',
            mode: 'ios',
            buttons: [
                {
                    text: dissmissButtonText,
                    role: 'cancel',
                    handler: dissmissButtonHandler
                },
                {
                    text: okButtonText,
                    handler: okButtonHandler
                }
            ]
        });
        await this.alert.present();
    }

    /**
     * Presents an alert that contains only one button
     * Generally used to inform the user about something that does not require a user action
     * @param msg Message to contain in the alert view
     * @param okButtonText Text for the button
     * @param dismissable True if the user can tap on the background to dismiss the alert
     * @param okButtonHandler Function to call when the button is pressed
     */
    async presentWithoutDismiss(msg: string, okButtonText: string, dismissable: boolean, okButtonHandler: () => void) {
        this.alert = await this.alertController.create({
            backdropDismiss: dismissable,
            message: msg,
            cssClass: 'custom-alert',
            mode: 'ios',
            buttons: [
                {
                    text: okButtonText,
                    handler: okButtonHandler
                }
            ]
        });
        await this.alert.present();
    }

    /**
     * Dismisses the alert
     */
    async dismiss() {
        if (this.alert) {
            await this.alert.dismiss();
            this.alert = null;
        }
    }

}