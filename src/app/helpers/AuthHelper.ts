import { Injectable } from "@angular/core";
import { NavController } from '@ionic/angular';
import { UserPreferencesHelper } from './UserPreferencesHelper';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { DatabaseHelper } from './DatabaseHelper';

@Injectable()
export class AuthHelper {

    // Current auth state of the user (true = authenticated)
    authState: BehaviorSubject<boolean> = new BehaviorSubject(false);
    configState: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(private userPreferencesHelper: UserPreferencesHelper, private navCtrl: NavController, private dbHelper: DatabaseHelper) {
    }
    /**
     * Checks if the user is logged in
     * @returns Promise when checking is done
     */
    public checkUserLoggedIn(): Promise<any> {
        return new Promise((res, err) => {
            this.userPreferencesHelper.get(UserPreferencesHelper.LOGGED_IN).then(loggedIn => {
                this.authState.next(loggedIn === 'true');
            }).finally(() => {
                res(true);
            });
        });
    }

    public checkConfigState(): Promise<any> {
        return new Promise((res, err) => {
            this.userPreferencesHelper.get(UserPreferencesHelper.CONFIG_STATE).then(configState => {
                this.configState.next(configState === 'true');
            }).finally(() => {
                res(true);
            });
        });
    }

    /**
     * 
     * @param val Value of new authState
     */
    public userAuthenticated(val: boolean) {
        this.authState.next(val);
        this.userPreferencesHelper.set(UserPreferencesHelper.LOGGED_IN, val.toString());
    }

    public stationConfigured(val: boolean) {
        this.configState.next(val);
        this.userPreferencesHelper.set(UserPreferencesHelper.CONFIG_STATE, val.toString());
    }

    /**
     * @returns Current authState value
     */
    public isAuthenticated(): boolean {
        return this.authState.value;
    }

    public isConfigured(): boolean {
        return this.configState.value;
    }
}

@Injectable({
    providedIn: 'root'
})
/**
 * This class acts like a guard that prevents users from accessing pages that they are not supposed to when not logged in.
 * Redirects users to login page if unauthorised access occurs
 */
export class AuthGuard implements CanActivate {

    path: ActivatedRouteSnapshot[];
    route: ActivatedRouteSnapshot;

    constructor(private authHelper: AuthHelper, private navCtrl: NavController) {

    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.authHelper.isConfigured();
    }

}