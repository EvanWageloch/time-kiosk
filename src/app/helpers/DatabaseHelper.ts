import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserPreferencesHelper } from './UserPreferencesHelper';
import { BehaviorSubject } from 'rxjs';
import { AlertHelper } from './AlertHelper';
import { Network } from '@ionic-native/network/ngx';
import { ClockConfig } from '../classes/ClockConfig';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { UtilityHelper } from './UtilityHelper';
import { Platform } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { FaceRecognitionHelper } from './FaceRecognitionHelper';

// Function from assets/js/sha_mobileapp.js 
// Used for hashing user passwords
declare function SHA256_hash(msg): string;
declare let cordova: any;

@Injectable()
/**
 * This is where all the endpoint calls are made.
 * The general state of the application is stored here during runtime.
 */
export class DatabaseHelper {

    user: any = {};

    sitesInfo: any = [];

    private apiHost: string = environment.apiHost;

    private api: string = 'api/timeAPI/';
    private apiUrl: string = this.apiHost + this.api; // Main api url

    public isOnline: BehaviorSubject<boolean>; // True if the user is online

    public pendingLogs: any[] = []; // Pending logs that need to be submitted
    public pendingNotes: any[] = []; // Pending notes that need to be submitted

    public clockConfig: ClockConfig = new ClockConfig();
    public staffList: any = null;

    public tokens = { Token: "", RefreshToken: "" };

    public clockConfigInterval;

    constructor(public httpClient: HttpClient, private userPreferencesHelper: UserPreferencesHelper, private faceRecognitionHelper: FaceRecognitionHelper, private alertHelper: AlertHelper, private network: Network, private router: Router, private utHelper: UtilityHelper, private platform: Platform, public device: Device, private appVersion: AppVersion) {
    }

    /**
     * Loads all the locally saved data for the current user
     */
    public async loadSavedData(): Promise<any> {
        return new Promise(async (res, err) => {
            try {
                this.pendingLogs = await this.userPreferencesHelper.get(UserPreferencesHelper.PENDING_LOGS);
                this.pendingNotes = await this.userPreferencesHelper.get(UserPreferencesHelper.PENDING_NOTES);
                this.clockConfig = await this.userPreferencesHelper.get(UserPreferencesHelper.CLOCK_CONFIG);
                this.staffList = await this.userPreferencesHelper.get(UserPreferencesHelper.STAFF_LIST);
                this.tokens = await this.userPreferencesHelper.get(UserPreferencesHelper.TOKENS);
                this.checkDarkMode();
                if (!this.pendingLogs) this.pendingLogs = [];
                if (!this.pendingNotes) this.pendingNotes = [];
                if (!this.tokens) this.tokens = { Token: "", RefreshToken: "" };
                this.submitPendingLogs();
            } catch (err) {
                // Something does not exist in the local storage, probably because the user just installed the app
            } finally {
                res(true);
            }
        });

    }

    /**
     * Main function to make calls to endpoints
     * @param endpoint Endpoint to make the call to
     * @param requestType Type of request GET | POST
     * @param body Parameters to pass to the request
     */
    private async performWebRequest(endpoint: string, requestType: string, body, checkToken: boolean, checkOnline: boolean = true): Promise<any> {

        // console.log("Check if app is online: " + checkOnline);
        if (!navigator.onLine || !this.isOnline.value) {
            console.log('App is offline.');
            if (checkOnline) {
                this.alertHelper.presentWithoutDismiss('Sorry the Wageloch server is not available, please check your connection.', 'OK', false, () => {
                    this.alertHelper.dismiss();
                });
            }
            return;
        }

        var url: string = this.apiUrl + endpoint;
        console.log('Web Service Request: ' + url);
        console.log('Web Service Body: ' + JSON.stringify(body));

        if (checkToken) {
            const tokens = await this.checkTokens();
            console.log("TOKENS: " + tokens);
            if (tokens === 'EXPIRED') {
                return;
            }
        }

        var headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8',
            'Cache-Control': 'no-cache'
        });

        var options = {
            headers: headers,
            body: body
        };

        return this.httpClient.request(requestType, url, options).toPromise();
    }

    public async checkTokens(): Promise<any> {
        return new Promise(async (res, rej) => {
            this.performWebRequest('timeCheckToken', 'POST', this.tokens, false).then(async val => {
                if (val === 'EXPIRED') {
                    console.log('Refreshing tokens...');
                    this.performWebRequest('timeRefreshToken', 'POST', this.tokens, false).then(async val2 => {
                        console.log(val2);
                        if (val2.ResultStr === 'EXPIRED') {
                            console.log('Refresh token also expired, resetting clock station...');
                            this.alertHelper.presentWithoutDismiss('Your session has expired. Please login again.', 'OK', false, async () => {
                                // Refresh token also expired, reset clock station
                                await this.TimeSetup_reset();
                                this.router.navigateByUrl('/login', { state: { nextRoute: "/configure", disableCancel: true } });
                            });
                            res('EXPIRED');
                        } else {
                            this.saveTokens(val2);
                            res('OK');
                        }
                    });
                } else {
                    res(val);
                }
            })
        });
    }

    public saveTokens(data) {
        this.tokens.Token = data.Token;
        this.tokens.RefreshToken = data.RefreshToken;
        this.userPreferencesHelper.set(UserPreferencesHelper.TOKENS, this.tokens);
    }

    public async createAuthInfo(IsLox, UseToken, IsAdmin, Email?, Password?) {
        var auth = { email: Email, password: Password };
        if (IsAdmin && !Email) auth = await this.userPreferencesHelper.get('auth');
        return {
            IsLox: IsLox,
            UseToken: UseToken,
            IsAdmin: IsAdmin,
            Email: IsAdmin ? auth.email : '',
            Password: IsAdmin ? auth.password : '',
            Sitecode: this.clockConfig.SiteCode,
            PureAPI: this.clockConfig.PureAPI,
            Initialised: this.clockConfig.Initialised,
            ClockPointId: this.clockConfig.ClockPointID,
            Token: UseToken ? this.tokens.Token : ""
            // Token: ""
        }
    }

    public async Timesetup_Dologin(email: string, password: string): Promise<any> {
        return this.performWebRequest('timeCheckLogin', 'POST', await this.createAuthInfo(false, false, true, email, password), false);
    }

    public async Timesetup_GetCloudUsers(): Promise<any> {
        return this.performWebRequest('timeLoadCloudUsers', 'POST', await this.createAuthInfo(false, true, false), true);
    }

    public async TimeSetup_PopulateDropdowns(): Promise<any> {
        return this.performWebRequest('timeSetupGetLists', 'POST', await this.createAuthInfo(false, this.clockConfig.Initialised, true), this.clockConfig.Initialised);
    }

    public async TimeSetup_Save(newLocationName: string): Promise<any> {
        return this.performWebRequest('timeSaveSetup', 'POST', {
            Auth: await this.createAuthInfo(false, this.clockConfig.Initialised, true),
            SelectedLocationGUID: this.clockConfig.ClockPointID,
            NewLocationName: newLocationName,
            PreviousRefreshToken: this.tokens.RefreshToken,
            ClockMethod: this.clockConfig.ClockOnMethod,
            IsApp: true,
            ShowSurnamefirst: this.clockConfig.ShowSurnamefirst,
            ShowLastClockTime: this.clockConfig.ShowLastClockTime,
            EmailLateClockin: this.clockConfig.EmailLateClockin,
            EmailAddresses: this.clockConfig.EmailAddresses,
            LateClockinMins: this.clockConfig.LateClockinMins,
            DarkMode: this.clockConfig.DarkMode,
            ColourBlindAssist: this.clockConfig.ColourBlindAssist,
            ShowEmpSearch: this.clockConfig.ShowEmpSearch,
            FirebaseId: this.clockConfig.FireBaseId,
            DeviceID: this.getDeviceID(),
            DeviceType: this.device.platform,
            OSVersion: this.device.version,
            Device: this.device.model,
        }, this.clockConfig.Initialised);
    }

    public async Time_GetClockConfig(checkOnline: boolean = true) {
        return this.performWebRequest('timeGetConfig', 'POST', await this.createAuthInfo(false, true, true), false, checkOnline);
    }

    public saveClockConfigLocally() {
        this.userPreferencesHelper.set(UserPreferencesHelper.CLOCK_CONFIG, this.clockConfig);
    }

    public async TimeSetup_reset() {
        await this.userPreferencesHelper.clearPrefs();
        this.faceRecognitionHelper.clearAll();
        this.clockConfig = new ClockConfig();
        this.saveClockConfigLocally();
        this.userPreferencesHelper.set(UserPreferencesHelper.CONFIG_STATE, 'false');
        this.checkDarkMode();
    }

    public async Time_GetStaffList(checkOnline: boolean = true): Promise<any> {
        return new Promise(async (res, err) => {
            this.performWebRequest('timeGetStaffList', 'POST', await this.createAuthInfo(false, true, false), true, checkOnline).then(val => {
                if (val.ResultStr == 'OK') {
                    this.staffList = val;
                    console.log(this.staffList);
                    this.userPreferencesHelper.set(UserPreferencesHelper.STAFF_LIST, this.staffList);
                }
                res(val);
            }).catch(e => err(e));
        });
    }

    public async Time_GetUnfilteredStaffList(checkOnline: boolean = true): Promise<any> {
        return new Promise(async (res, err) => {
            this.performWebRequest('timeGetUnfilteredStaffList', 'POST', await this.createAuthInfo(false, true, false), true, checkOnline).then(val => {
                res(val);
            }).catch(e => err(e));
        });
    }

    public async Staff_ResetPassword(staffCode): Promise<any> {
        return new Promise(async (res, err) => {
            this.performWebRequest('timeResetPassword', 'POST', {
                Auth: await this.createAuthInfo(false, true, true),
                StaffCode: staffCode,
                Password: ''
            }, true).then(val => {
                res(val);
            }).catch(e => err(e));
        });
    }

    public Staff_CheckPassword(staff, password) {
        var shapwd = this.Staff_GetSHAPassword(password);
        var staffSha = staff.shaPassword ? staff.shaPassword : this.Staff_GetSHAPassword(staff.ClockPassword);
        if (shapwd !== staffSha) {
            return false;
        }
        return true;
    }

    public Staff_GetSHAPassword(password) {
        return SHA256_hash(this.clockConfig.SiteCode + password);
    }

    public Staff_HasPassword(staff): boolean {
        return staff.shaPassword !== null && staff.shaPassword !== '';
    }

    public Staff_UpdateClock(staff, body) {
        this.staffList.staffList.find((o, i) => {
            if (o.StaffCode === staff.StaffCode) {
                this.staffList.staffList[i].CurrentlyClockedIn = body.ClockType === 'in';
                this.staffList.staffList[i].LastClockTimeDisplay = this.utHelper.formatAMPM_ClockTime(new Date(body.dLastClockTime));
                staff.CurrentlyClockedIn = body.ClockType === 'in';
                staff.LastClockTimeDisplay = this.utHelper.formatAMPM_ClockTime(new Date(body.dLastClockTime));
                this.userPreferencesHelper.set(UserPreferencesHelper.STAFF_LIST, this.staffList);
                return true;
            }
        });
    }

    public async Time_PostClockTime_Body(staff, dept: string, role: string, admin: boolean, editing: boolean): Promise<any> {
        var facePwd = 'face~' + this.getDeviceID();
        const body = {
            Auth: await this.createAuthInfo(false, true, false),
            SiteCode: this.clockConfig.SiteCode,
            StaffCode: staff.StaffCode,
            Password: staff.usingFace ? facePwd : staff.ClockPassword,
            Dept: dept,
            Role: role,
            clockTime: this.utHelper.formattedTime(new Date()),
            dLastClockTime: new Date().toISOString(),
            ClockType: staff.CurrentlyClockedIn ? 'out' : 'in',
            shaPassword: staff.shaPassword,
            AdminEntry: admin,
            ClockIndex: editing ? 1 : -1,
            ClockPointID: this.clockConfig.ClockPointID
        }
        return body;
    }

    public async Time_PostClockTime(body): Promise<any> {
        if (!navigator.onLine || !this.isOnline.value) {
            console.log('No internet, saving clock log.');
            this.pendingLogs.push(body);
            this.userPreferencesHelper.set(UserPreferencesHelper.PENDING_LOGS, this.pendingLogs);
        } else {
            return this.performWebRequest('timePostClocktime', 'POST', body, true);
        }
    }

    public async Time_PostClockTimeNote_Body(staff, description, note): Promise<any> {
        const body = {
            Auth: await this.createAuthInfo(false, true, false),
            SiteCode: this.clockConfig.SiteCode,
            StaffCode: staff.StaffCode,
            Password: staff.ClockPassword,
            DateTime: new Date(),
            Description: description,
            Note: note,
        }
        return body;
    }

    public async Time_PostClockTimeNote(body) {
        if (!navigator.onLine || !this.isOnline.value) {
            console.log('No internet, saving note.');
            this.pendingNotes.push(body);
            this.userPreferencesHelper.set(UserPreferencesHelper.PENDING_NOTES, this.pendingNotes);
        } else {
            return this.performWebRequest('timePostClockTimeNote', 'POST', body, true);
        }
    }

    public async Time_GetClockTimes(date: string): Promise<any> {
        return this.performWebRequest('timeGetClockTimes', 'POST', { Auth: await this.createAuthInfo(false, true, false), ForDate: date }, true, true);
    }

    public async Time_PostClockTimeAdmin(type: 'delday' | 'del' | 'upd', staff, dept: string, role: string, time: string, clockIndex: number): Promise<any> {
        return this.performWebRequest('timePostClockTimeAdmin', 'POST', {
            Auth: await this.createAuthInfo(false, true, false),
            SiteCode: this.clockConfig.SiteCode,
            StaffCode: staff.StaffCode,
            Password: '',
            Dept: dept,
            Role: role,
            clockTime: time,
            ClockType: type,
            shaPassword: '',
            AdminEntry: true,
            ClockIndex: clockIndex,
            ClockPointID: this.clockConfig.ClockPointID,
        }, true, true);
    }

    /**
     * Submits logs that were pending due to no connectivity
     */
    public submitPendingLogs() {
        console.log('Pending logs: ' + this.pendingLogs.length);
        if (this.pendingLogs && this.pendingLogs.length > 0 && navigator.onLine && this.isOnline.value) {
            const promises: Promise<any>[] = [];
            this.pendingLogs.forEach(log => {
                promises.push(this.Time_PostClockTime(log));
            });
            Promise.all(promises).finally(() => {
                // Delay the get staff list call, just incase there is another call running already.
                setTimeout(() => {
                    this.Time_GetStaffList();
                }, 1000);
            });
            this.pendingLogs = [];
            this.userPreferencesHelper.set(UserPreferencesHelper.PENDING_LOGS, this.pendingLogs);
        }
        console.log('Pending notes: ' + this.pendingNotes.length);
        if (this.pendingNotes && this.pendingNotes.length > 0 && navigator.onLine && this.isOnline.value) {
            this.pendingNotes.forEach(log => {
                this.Time_PostClockTimeNote(log);
            });
            this.pendingNotes = [];
            this.userPreferencesHelper.set(UserPreferencesHelper.PENDING_NOTES, this.pendingNotes);
        }
    }

    public setupClockConfigInterval() {
        this.Time_GetClockConfig(false).then(val => {
            console.log(val);
            if (val.ResultStr === 'OK') {
                this.clockConfig = val;
                this.saveClockConfigLocally();
            }
        });
        this.clockConfigInterval = setInterval(() => {
            this.Time_GetClockConfig(false).then(val => {
                if (val.ResultStr === 'OK') {
                    this.clockConfig = val;
                    this.saveClockConfigLocally();
                }
            });
        }, 1000 * 60 * (this.clockConfig.ConfigRefreshIntervalMinutes ? this.clockConfig.ConfigRefreshIntervalMinutes : 120));
    }

    /**
     * Setup listeners for is user online
     * Inform the user when the Internet connection is lost.
     */
    public setupIsOnline() {
        this.isOnline = new BehaviorSubject(navigator.onLine);

        this.network.onDisconnect().subscribe(() => {
            console.log("Network Disconnected");
            this.isOnline.next(false);
        });

        this.network.onConnect().subscribe(() => {
            console.log("Network Connected");
            this.isOnline.next(true);
            this.submitPendingLogs();
        });
    }

    checkIfUpdateAvailable(): Promise<boolean> {
        if (!this.device.cordova) return Promise.resolve(false);
        return new Promise(async (res) => {
            this.performWebRequest('timeGetAppInfo', 'POST', {
                Token: this.tokens.Token,
                AppStoreId: await this.appVersion.getPackageName(),
                DeviceType: this.device.platform,
            }, false, false).then(async val => {
                if (val.ResultStr === 'OK') {
                    res(val.Version !== await this.appVersion.getVersionNumber());
                } else {
                    res(false);
                }
            }).catch(() => {
                res(false);
            });
        });
    }

    isStorageLow(): Promise<boolean> {
        return new Promise((res) => {
            this.getAvailableSpace().then(val => {
                var diskSizeInMB = val / 1024 / 1024;
                res(diskSizeInMB < 200);
            });
        });
    }

    /**
     * Copied as it is from the old app
     * @param item Email
     */
    private getSalt(item): string {
        var salt = "";
        var vowels = 'AEIOUaeiou';
        item = item.toLowerCase();
        for (var x = 0; x < item.length; x++) {
            var c = item.charAt(x);
            if (vowels.indexOf(c) < 0)
                salt = c + salt;
        }
        return salt;
    }

    /**
     * Save a user's details locally
     * @param user User to save
     */
    public saveUser(user: any) {
        this.user = user;
        // this.userPreferencesHelper.set(UserPreferencesHelper.USER, JSON.stringify(user));
    }

    /**
     * Logout
     */
    public async logoutUser() {
        return true;
        // return this.deregisterDeviceForPush(await this.userPreferencesHelper.get(UserPreferencesHelper.FCM_TOKEN));
    }

    /**
     * Retrieves the site name with the specified site code.
     * @param sitecode Site code to search for
     */
    public getSiteNameFromCode(sitecode: string): string {
        for (var i = 0; i < this.sitesInfo.length; i++) {
            if (this.sitesInfo[i].SiteCode === sitecode) {
                return this.sitesInfo[i].Sitename;
            }
        }
    }

    /**
     * Retrieves the site code with the specified site name.
     * @param sitename Site name to search for
     */
    public getSiteCodeFromName(sitename: string): string {
        for (var i = 0; i < this.sitesInfo.length; i++) {
            if (this.sitesInfo[i].Sitename === sitename) {
                return this.sitesInfo[i].SiteCode;
            }
        }
    }

    public getStaffFromStaffCode(staffCode: string) {
        for (var i = 0; i < this.staffList.staffList.length; i++) {
            if (this.staffList.staffList[i].StaffCode === staffCode) {
                return this.staffList.staffList[i];
            }
        }
    }

    public checkDarkMode() {
        document.body.classList.toggle('dark', this.clockConfig.DarkMode);
        document.body.classList.toggle('color-blind-assist', this.clockConfig.ColourBlindAssist);
    }

    public getAvailableSpace(): Promise<number> {
        return new Promise((res, rej) => {
            if (this.platform.is('cordova')) {
                if (this.platform.is('android')) {
                    res(cordova.plugins['extended-device-information'].freestorage * 1024 * 1024);
                } else {
                    cordova.exec((result) => {
                        res(result);
                    }, (error) => {
                        res(0);
                    }, "File", "getFreeDiskSpace", []);
                }
            } else {
                res(0);
            }
        });
    }

    public getDeviceID(): string {
        return !this.device.cordova ? 'Virtual Device' : this.device.uuid;
    }
}