import { Injectable } from "@angular/core";
import { ModalController } from '@ionic/angular';
import { LoadingSpinnerComponent } from '../components/loading-spinner/loading-spinner.component';


@Injectable()
/**
 * Class for handing a custom Loading View service.
 * Provides methods for creating, present and dismissing loading views.
 */
export class LoadingHelper {

    private loadingView: HTMLIonModalElement;

    private errCount = 0;

    constructor(private modalController: ModalController) {
    }

    async createLoadingView() {
        this.loadingView = await this.modalController.create({
            component: LoadingSpinnerComponent,
            cssClass: "loading-modal",
            backdropDismiss: false,
            mode: 'ios',
        });
    }

    async present() {
        await this.createLoadingView();
        this.loadingView.present();
        this.errCount = 0;
    }

    dismiss() {
        if (!this.loadingView) {
            // If someone else already dismissed it or not yet created, retry after 500ms.
            setTimeout(() => {
                this.errCount++;
                if (this.errCount < 5) {
                    this.dismiss();
                }
            }, 500);
        }
        // Dismiss loading view
        setTimeout(() => {
            this.modalController.dismiss().then(() => {
                this.loadingView = null;
            }).catch(() => {
                
            });
        }, 500);
    }
}