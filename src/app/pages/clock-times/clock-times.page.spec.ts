import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClockTimesPage } from './clock-times.page';

describe('ClockTimesPage', () => {
  let component: ClockTimesPage;
  let fixture: ComponentFixture<ClockTimesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClockTimesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClockTimesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
