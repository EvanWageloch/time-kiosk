import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PickerController } from '@ionic/angular';
import { AlertHelper } from 'src/app/helpers/AlertHelper';
import { DatabaseHelper } from 'src/app/helpers/DatabaseHelper';
import { LoadingHelper } from 'src/app/helpers/LoadingHelper';
import { UtilityHelper } from 'src/app/helpers/UtilityHelper';

@Component({
  selector: 'app-clock-times',
  templateUrl: './clock-times.page.html',
  styleUrls: ['./clock-times.page.scss'],
})
export class ClockTimesPage implements OnInit {

  items = [];
  allItems = [];

  selectedDate = new Date().toISOString();
  maxDate = new Date().toISOString();

  selectedTime = new Date().toISOString();

  numberOfColumns = [];

  selectedIndex = -1;
  editSelectedIndex = -1;
  editing = false;

  creating = false;

  deptSearchTerm = '';
  roleSearchTerm = '';

  showDeptRolePrompt = false;
  showNotesPrompt = false;
  selectedDeptForClock = '';
  selectedRoleForClock = '';
  note = '';
  noteSent = false;

  showTimePrompt = false;

  error = '';

  selectedStaff: any;
  selectedStaffFull: any;

  loading = false;

  constructor(public dbHelper: DatabaseHelper, public utHelper: UtilityHelper, private loadingHelper: LoadingHelper, private router: Router, private alertHelper: AlertHelper, private pickerController: PickerController) { }

  ngOnInit() {
    var date = new Date();
    date.setHours(23, 59, 59);
    this.maxDate = date.toISOString();
    this.loadClockTimes();
  }

  loadClockTimes(presentLoading = true) {
    if (this.loading) return;
    this.loading = true;
    if (presentLoading) this.loadingHelper.present();
    this.dbHelper.Time_GetClockTimes(this.utHelper.dateWithHiphen(this.selectedDate)).then(val => {
      this.items = [];
      console.log(val);
      if (val.ResultStr === 'OK') {
        this.numberOfColumns = [...Array(val.InOutHeadingCount).keys()];
        this.allItems = val.Items;
        val.Items.forEach(element => {
          if (element.ClockEntry) {
            this.items.push(element);
          }
        });
        if (this.editing) {
          var tempI = this.selectedIndex;
          this.selectedIndex = -1;
          this.selectStaff(tempI);
        } else {
          this.selectedIndex = -1;
        }
      }
    }).finally(() => {
      this.loadingHelper.dismiss();
      this.loading = false;
    });
  }

  selectStaff(i) {
    if (this.selectedIndex === i) {
      this.selectedIndex = -1;
    } else {
      this.selectedIndex = i;
      this.selectedStaff = this.items[i];
      this.selectedStaffFull = this.dbHelper.staffList.staffList.find(staff => staff.StaffCode === this.selectedStaff.StaffCode);
    }
  }

  edit() {
    this.editing = true;
    this.editSelectedIndex = -1;
  }

  editing_delete() {
    if (this.selectedStaff.ClockEntry[this.editSelectedIndex].IsAdded) {
      this.alertHelper.present('Are you sure you want to delete the selected manual entry?', 'Delete', 'Cancel', true, () => {
        this.loadingHelper.present();
        this.dbHelper.Time_PostClockTimeAdmin('del', this.selectedStaffFull, this.selectedDeptForClock, this.selectedRoleForClock, this.selectedStaff.ClockEntry[this.editSelectedIndex].ClockTime, this.selectedStaff.ClockEntry[this.editSelectedIndex].ClockIndex).then(val => {
          this.loadClockTimes(false);
          this.editSelectedIndex = -1;
          if (this.getEntries(this.selectedStaff).length === 1) {
            this.clearSelectedStaff();
          }
        });
      }, null);
    } else {
      this.alertHelper.presentWithoutDismiss('Only manual clock times can be deleted, please select a manual clock time.', 'OK', true, null);
    }
  }

  editing_edit() {
    this.creating = false;
    if (this.selectedStaff.ClockEntry[this.editSelectedIndex].IsAdded) {
      this.showTimePrompt = true;
      this.selectedTime = this.utHelper.parseFormattedTime(this.selectedStaff.ClockEntry[this.editSelectedIndex].ClockTime, '-').toISOString();
    } else {
      this.alertHelper.presentWithoutDismiss('Only manual clock times can be edited, please select a manual clock time.', 'OK', true, null);
    }
  }

  editing_create() {
    this.creating = true;
    if (this.dbHelper.clockConfig.ShowDeptRolePrompt && (this.dbHelper.clockConfig.UseDeptForClock || this.dbHelper.clockConfig.UseRoleForClock)) {
      this.selectedDeptForClock = '';
      this.selectedRoleForClock = '';
      this.deptSearchTerm = '';
      this.roleSearchTerm = '';
      this.showDeptRolePrompt = true;
    } else {
      this.selectedTime = new Date(this.selectedDate).toISOString();
      console.log(this.selectedTime);
      this.showTimePrompt = true;
    }
  }

  editing_done() {
    if (!this.creating) {
      this.clockStaff(this.selectedStaff.ClockEntry[this.editSelectedIndex].ClockIndex);
    } else {
      this.clockStaff(-1);
      this.clearSelectedStaff();
    }
    this.showTimePrompt = false;
    this.showDeptRolePrompt = false;
  }

  timePrompt_cancel() {
    this.showTimePrompt = false;
  }

  deptRolePicked() {
    this.error = '';
    if (this.dbHelper.clockConfig.UseDeptForClock && this.selectedDeptForClock === '') {
      this.error = 'Please select your department.';
      return;
    }
    if (this.dbHelper.clockConfig.UseRoleForClock && this.selectedRoleForClock === '') {
      this.error = 'Please select your role.';
      return;
    }
    this.selectedTime = new Date(this.selectedDate).toISOString();
    console.log(this.selectedTime);
    this.showTimePrompt = true;
  }

  async clockStaff(clockIndex) {
    this.loadingHelper.present();
    this.dbHelper.Time_PostClockTimeAdmin('upd', this.selectedStaffFull, this.selectedDeptForClock, this.selectedRoleForClock, this.utHelper.formattedTime(this.selectedTime), clockIndex).then(val => {
      console.log(val);
      this.loadClockTimes(false);
    });
  }

  async sendNote() {
    this.note = this.note.trim();
    var description = 'Daily note';
    this.error = '';
    if (this.dbHelper.clockConfig.PromptForNote) {
      if (!this.note) {
        this.error = 'Please enter your note';
        return;
      }

    } else {
      description = 'No lunch break';
      this.note = 'No lunch break was taken by ' + this.selectedStaff.StaffName;
    }

    const body = await this.dbHelper.Time_PostClockTimeNote_Body(this.selectedStaffFull, description, this.note);
    this.dbHelper.Time_PostClockTimeNote(body).then(val => {
      console.log("Note sent: " + JSON.stringify(val));
    });
    this.showNotesPrompt = false;
    this.noteSent = true;
    this.note = '';
  }

  clearSelectedStaff() {
    this.selectedIndex = -1;
    this.editing = false;
    this.creating = false;
    this.selectedStaff = null;
    this.selectedStaffFull = null;
    this.showDeptRolePrompt = false;
    this.showNotesPrompt = false;
    this.showTimePrompt = false;
    this.selectedDeptForClock = '';
    this.selectedRoleForClock = '';
    this.deptSearchTerm = '';
    this.roleSearchTerm = '';
    this.noteSent = false;
    this.note = '';
    this.selectedTime = new Date().toISOString();
  }

  previousDay() {
    var date = new Date(this.selectedDate);
    date.setDate(date.getDate() - 1);
    this.selectedDate = date.toISOString();
    // this.loadClockTimes();
  }

  nextDay() {
    var date = new Date(this.selectedDate);
    date.setDate(date.getDate() + 1);
    this.selectedDate = date.toISOString();
    // this.loadClockTimes();
  }

  today() {
    this.selectedDate = new Date().toISOString();
    // this.loadClockTimes();
  }

  getEntries(staff) {
    var entries = [];
    for (var i = 0; i < staff.ClockEntry.length; i++) {
      var entry = staff.ClockEntry[i];
      if (i === 0) {
        if (!entry.IsClockIn) {
          entries.push(null);
        }
      } else {
        var previousEntry = staff.ClockEntry[i - 1];
        if (entry.IsClockIn === previousEntry.IsClockIn) {
          entries.push(null);
        }
      }
      entries.push(entry);
    }
    return entries;
  }

  done() {
    this.router.navigateByUrl('/');
  }

  delete() {
    var containsOnlyManualEntries = true;
    this.items[this.selectedIndex].ClockEntry.forEach(entry => {
      if (!entry.IsAdded) {
        containsOnlyManualEntries = false;
      }
    });
    if (!containsOnlyManualEntries) {
      this.alertHelper.presentWithoutDismiss('Only manual clock times can be deleted, please select a manual clock time.', 'OK', true, null);
    } else {
      this.alertHelper.present('Are you sure you want to delete the selected manual entries?', 'Delete', 'Cancel', true, () => {
        this.loadingHelper.present();
        this.dbHelper.Time_PostClockTimeAdmin('delday', this.selectedStaffFull, this.selectedDeptForClock, this.selectedRoleForClock, this.utHelper.formattedTime(this.selectedDate), -1).then(val => {
          this.loadClockTimes(false);
        });
      }, null);
    }
  }

  async presentStaffPicker() {
    var options = [];
    this.allItems.forEach(staff => {
      options.push({
        text: staff.StaffName,
        value: staff.StaffCode
      });
    });

    const picker = await this.pickerController.create({
      animated: true,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Select',
          handler: (val) => {
            this.selectedStaff = this.allItems.find(staff => staff.StaffCode === val.options.value);
            this.selectedStaffFull = this.dbHelper.staffList.staffList.find(staff => staff.StaffCode === this.selectedStaff.StaffCode);
            this.editSelectedIndex = -1;
          }
        }
      ],
      columns: [
        {
          name: 'options',
          options: options
        }
      ],
      cssClass: 'custom-picker',
      mode: 'ios',
    });
    picker.columns[0].options.forEach(element => {
      delete element.selected;
      delete element.duration;
      delete element.transform;
    });
    picker.present();
  }

  get showNextDay() {
    return (this.utHelper.dateWithHiphen(this.selectedDate) !== this.utHelper.dateWithHiphen(new Date()));
  }

  roles() {
    var arr = [];
    arr = this.dbHelper.clockConfig.AvailableRoles;
    if (this.dbHelper.clockConfig.UseLinkedRoles && this.selectedDeptForClock) {
      if (Object.keys(this.dbHelper.clockConfig.LinkedRoles).includes(this.selectedDeptForClock)) {
        arr = this.dbHelper.clockConfig.LinkedRoles[this.selectedDeptForClock];
      } else {
        arr = this.dbHelper.clockConfig.AvailableRoles;
      }
      if (!arr.includes(this.selectedRoleForClock)) {
        this.selectedRoleForClock = '';
      }
    }
    arr.sort();

    // Search
    if (this.roleSearchTerm.trim()) {
      arr = arr.filter(item => item.toLowerCase().startsWith(this.roleSearchTerm.trim().toLowerCase()));
    }

    if (!arr.includes(this.selectedRoleForClock)) {
      this.selectedRoleForClock = '';
    }

    return arr;
  }

  departments() {
    var arr = [];
    arr = this.selectedStaffFull.Departments;
    if (this.dbHelper.clockConfig.UseLinkedRoles && this.selectedRoleForClock) {
      arr = [];
      Object.keys(this.dbHelper.clockConfig.LinkedRoles).forEach(key => {
        if (this.selectedStaffFull.Departments.includes(key) && this.dbHelper.clockConfig.LinkedRoles[key].includes(this.selectedRoleForClock)) {
          arr.push(key);
        }
      });
      this.selectedStaffFull.Departments.forEach(el => {
        if (!Object.keys(this.dbHelper.clockConfig.LinkedRoles).includes(el) && !arr.includes(el)) {
          arr.push(el);
        }
      });
      if (arr.length === 0) {
        arr = this.selectedStaffFull.Departments;
      }
      if (!arr.includes(this.selectedDeptForClock)) {
        this.selectedDeptForClock = '';
      }
    }
    arr.sort();

    // Search
    if (this.deptSearchTerm.trim()) {
      arr = arr.filter(item => item.toLowerCase().startsWith(this.deptSearchTerm.trim().toLowerCase()));
    }

    if (!arr.includes(this.selectedDeptForClock)) {
      this.selectedDeptForClock = '';
    }

    return arr;
  }
}
