import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClockTimesPageRoutingModule } from './clock-times-routing.module';

import { ClockTimesPage } from './clock-times.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClockTimesPageRoutingModule
  ],
  declarations: [ClockTimesPage]
})
export class ClockTimesPageModule {}
