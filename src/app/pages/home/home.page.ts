import { Component, ChangeDetectorRef, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PopoverController, PickerController, AnimationController } from '@ionic/angular';
import { SettingsDropdownComponent } from 'src/app/components/settings-dropdown/settings-dropdown.component';
import { UtilityHelper } from 'src/app/helpers/UtilityHelper';
import { DatabaseHelper } from 'src/app/helpers/DatabaseHelper';
import { LoadingHelper } from 'src/app/helpers/LoadingHelper';
import { Printer } from '@ionic-native/printer/ngx';
import { AlertHelper } from 'src/app/helpers/AlertHelper';
import { Router } from '@angular/router';
import { FaceRecognitionHelper } from 'src/app/helpers/FaceRecognitionHelper';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  intervalHolder: any;
  refreshInterval: any;
  intervalClearDeptAndSearch: any;

  selectedDepartment = "All Departments";

  selectedStaff = null;
  showDeptRolePrompt = false;
  showNotesPrompt = false;
  selectedDeptForClock = '';
  selectedRoleForClock = '';
  note = '';
  noteSent = false;

  isClockingWithFace = false;

  searchTerm = '';
  deptSearchTerm = '';
  roleSearchTerm = '';

  clockMethod = this.dbHelper.clockConfig.ClockOnMethod === 100 ? 'PIN' : 'Password';
  changePassword = false;
  newPassword = '';
  confirmPassword = '';
  password = '';

  error = '';

  dropDownTimer;

  isStorageLow = false;
  storageExceeded = false;

  isUpdateAvailable = false;

  personalisedMessage = '';
  hidePersonalisedMessage = true;
  clockingIn = true;
  forcePIN = false;
  registeringFace = false;

  showingCameraFeed = false;
  adminCancelledCameraFeed = false;

  showCustomStaffList = false;
  customStaffList = [];

  customStaffListSelectedIndex = -1;

  @ViewChild("messageModal", { read: ElementRef, static: true }) messageModal: ElementRef;

  constructor(public dbHelper: DatabaseHelper, private popoverController: PopoverController, private utHelper: UtilityHelper, private changeDetector: ChangeDetectorRef, private pickerController: PickerController, private loadingHelper: LoadingHelper, private printer: Printer, private alertHelper: AlertHelper, private animationCtrl: AnimationController, private router: Router, private faceRecognitionHelper: FaceRecognitionHelper) { }

  ionViewWillEnter() {
    console.log("View will enter");
    this.clockMethod = this.dbHelper.clockConfig.ClockOnMethod === 100 ? 'PIN' : 'Password';
    this.dbHelper.Time_GetStaffList(false).finally(() => {
      setTimeout(() => {
        this.changeDetector.markForCheck();
      }, 1000);
    });

    this.dbHelper.isStorageLow().then(val => {
      this.isStorageLow = val;
    });
    this.dbHelper.getAvailableSpace().then(val => {
      var diskSizeInMB = val / 1024 / 1024;
      this.storageExceeded = diskSizeInMB < 5;
    });

    this.dbHelper.checkIfUpdateAvailable().then(val => {
      this.isUpdateAvailable = val;
    });

    this.intervalHolder = setInterval(() => {
      this.changeDetector.markForCheck();
    }, 1000 * 60);
    this.refreshInterval = setInterval(() => {
      this.dbHelper.Time_GetStaffList(false).finally(() => {
        this.changeDetector.markForCheck();
      });
      this.dbHelper.isStorageLow().then(val => {
        this.isStorageLow = val;
      });
      this.dbHelper.getAvailableSpace().then(val => {
        var diskSizeInMB = val / 1024 / 1024;
        this.storageExceeded = diskSizeInMB < 5;
      });
      this.dbHelper.checkIfUpdateAvailable().then(val => {
        this.isUpdateAvailable = val;
      });
      this.adminCancelledCameraFeed = false;
      this.checkClockingWithFace();
    }, 1000 * 60 * (this.dbHelper.clockConfig.StaffRefreshIntervalMinutes ? this.dbHelper.clockConfig.StaffRefreshIntervalMinutes : 5));
    this.intervalClearDeptAndSearch = setInterval(() => {
      this.selectedDepartment = 'All Departments';
      this.searchTerm = '';
      this.deptSearchTerm = '';
      this.roleSearchTerm = '';
      this.changeDetector.markForCheck();
    }, 1000 * (this.dbHelper.clockConfig.DeptFilterAndSearchboxesTimeoutSeconds ? this.dbHelper.clockConfig.DeptFilterAndSearchboxesTimeoutSeconds : 30));
    this.dbHelper.setupClockConfigInterval();
    this.checkClockingWithFace();
    this.changeDetector.markForCheck();
  }

  ionViewWillLeave() {
    console.log("View will leave");
    clearInterval(this.intervalHolder);
    clearInterval(this.refreshInterval);
    clearInterval(this.intervalClearDeptAndSearch);
    clearInterval(this.dbHelper.clockConfigInterval);
    if (this.dropDownTimer) clearTimeout(this.dropDownTimer);
  }

  ngOnInit(): void {
  }

  async selectStaff(staff) {
    console.log(staff);
    staff.usingFace = false;
    if (this.dbHelper.clockConfig.ClockOnMethod === 0) return;

    if (this.dbHelper.clockConfig.ClockOnMethod === 200) {
      this.showDeptRole(staff);
      return;
    }

    if (this.isClockingWithFace && staff.AlwaysPromptForPWDorPin) {
      this.isClockingWithFace = false;
      this.forcePIN = true;
      this.clockMethod = 'Password';
      this.selectedStaff = staff;
      return;
    }

    this.adminCancelledCameraFeed = false;
    if (this.dbHelper.clockConfig.ClockOnMethod === 300 || this.registeringFace) {
      if (this.showingCameraFeed) return;
      if (this.isRegistered(staff)) {
        try {
          this.showingCameraFeed = true;
          var val = await this.faceRecognitionHelper.login(this.dbHelper.clockConfig.DarkMode ? "true" : "false");
          this.showingCameraFeed = false;
          if (val.status === 'REGISTER') {
            this.showFaceRegistrationModal();
            return;
          } else if (val.status === 'PASSWORD') {
            this.showUsePasswordModal();
            return;
          } else if (val.status === 'CANCEL') {
            this.adminCancelledCameraFeed = true;
            return;
          }
          if (val.name === staff.StaffCode) {
            staff.usingFace = true;
            this.showDeptRole(staff);
          } else {
            this.showMessage(null, true, "Cannot recognise face. Please try again");
            return;
          }
        } catch (err) {
          this.showingCameraFeed = false;
          if (err.status === 'REGISTER') {
            this.showFaceRegistrationModal();
            return;
          } else if (err.status === 'PASSWORD') {
            this.showUsePasswordModal();
            return;
          } else if (err.status === 'CANCEL') {
            this.adminCancelledCameraFeed = true;
            return;
          } else {
            this.showMessage(null, true, "Cannot recognise face. Please try again");
          }
          return;
        }
      } else {
        this.registeringFace = false;
        try {
          this.showingCameraFeed = true;
          var val = await this.faceRecognitionHelper.register(staff.StaffCode, this.dbHelper.clockConfig.DarkMode ? "true" : "false");
          this.showingCameraFeed = false;
          if (val.status === 'REGISTER') {
            this.showFaceRegistrationModal();
            return;
          } else if (val.status === 'PASSWORD') {
            this.showUsePasswordModal();
            return;
          } else if (val.status === 'CANCEL') {
            this.adminCancelledCameraFeed = true;
            return;
          }
          if (val.status.toLocaleUpperCase() === "SUCCESS") {
            this.faceRecognitionHelper.saveRegisteredFace(staff.StaffCode, val.id);
            staff.usingFace = true;
            if (this.dbHelper.clockConfig.ClockOnMethod !== 500) {
              this.showDeptRole(staff);
            } else {
              this.clockStaffTouchless(staff);
            }
          } else {
            this.showMessage(null, true, "Cannot recognise face. Please try again");
          }
        } catch (err) {
          this.showingCameraFeed = false;
          if (err.status === 'REGISTER') {
            this.showFaceRegistrationModal();
            return;
          } else if (err.status === 'PASSWORD') {
            this.showUsePasswordModal();
            return;
          } else if (err.status === 'CANCEL') {
            this.adminCancelledCameraFeed = true;
            return;
          } else {
            if (err.message === 'Already registered') {
              this.showMessage(null, true, "Your face is already registered.");
            } else {
              this.showMessage(null, true, "Cannot recognise face. Please try again");
            }
          }
          return;
        }
      }
    } else {
      this.showCameraFeed();
    }

    this.selectedStaff = staff;
  }

  showDeptRole(staff) {
    if (this.dbHelper.clockConfig.ShowDeptRolePrompt && (this.dbHelper.clockConfig.UseDeptForClock || this.dbHelper.clockConfig.UseRoleForClock)
      && ((!staff.CurrentlyClockedIn && this.dbHelper.clockConfig.WhenToshowPrompt === 'IN') || (staff.CurrentlyClockedIn && this.dbHelper.clockConfig.WhenToshowPrompt === 'OUT'))) {
      this.selectedStaff = staff;
      if (this.dbHelper.clockConfig.UseDeptForClock) {
        this.selectedDeptForClock = this.selectedStaff.Departments[0];
      }
      this.showDeptRolePrompt = true;
    } else {
      this.clockStaff(staff, this.selectedDeptForClock, this.selectedRoleForClock, false);
    }
  }

  clockStaffTouchless(staff) {
    if (this.dbHelper.clockConfig.ShowDeptRolePrompt) {
      if (this.dbHelper.clockConfig.UseRoleForClock) {
        this.selectedRoleForClock = this.dbHelper.clockConfig.AvailableRoles[0];
      }
      if (this.dbHelper.clockConfig.UseDeptForClock && staff.Departments && staff.Departments.length > 0) {
        this.selectedDeptForClock = staff.Departments[0];
        if (this.dbHelper.clockConfig.UseLinkedRoles && Object.keys(this.dbHelper.clockConfig.LinkedRoles).includes(this.selectedDeptForClock)) {
          this.dbHelper.clockConfig.LinkedRoles[this.selectedDeptForClock][0];
        }
      }
    }
    this.clockStaff(staff, this.selectedDeptForClock, this.selectedRoleForClock, false);
  }

  async clockStaff(staff, dept, role, checkDeptRoles) {
    this.error = '';
    if (checkDeptRoles) {
      if (this.dbHelper.clockConfig.UseDeptForClock && dept === '') {
        this.error = 'Please select your department.';
        return;
      }
      if (this.dbHelper.clockConfig.UseRoleForClock && role === '') {
        this.error = 'Please select your role.';
        return;
      }
    }
    this.loadingHelper.present();
    const body = await this.dbHelper.Time_PostClockTime_Body(staff, dept, role, false, false);
    this.dbHelper.Time_PostClockTime(body).then(val => {
      this.dbHelper.Staff_UpdateClock(staff, body);
      this.loadingHelper.dismiss();
      const staffCopy = JSON.parse(JSON.stringify(staff));
      setTimeout(() => {
        this.dbHelper.Time_GetStaffList(false);
        this.showMessage(staffCopy);
      }, 1000);
      this.searchTerm = '';
      this.clearSelectedStaff();
    });
  }

  closeDeptRolePrompt() {
    this.showDeptRolePrompt = false;
    this.clearSelectedStaff();
    this.checkClockingWithFace(0);
  }

  async checkPassword() {
    this.error = '';
    if (!this.selectedStaff.shaPassword || this.changePassword) {
      if (!this.newPassword) {
        this.error = 'Please enter a new ' + this.clockMethod + '.';
        return;
      } else if ((this.clockMethod === 'Password' || this.dbHelper.clockConfig.ClockOnMethod === 1) && this.newPassword.length < 5) {
        this.error = 'Password should be at least 5 characters.'
        return;
      } else if (this.dbHelper.clockConfig.ClockOnMethod === 100 && this.newPassword.length != 4) {
        this.error = 'Pin code must be 4 numbers.';
        return;
      } else if (this.newPassword !== this.confirmPassword) {
        this.error = 'Your ' + this.clockMethod + ' and confirmation ' + this.clockMethod + ' do not match.';
        return;
      } else if (this.changePassword && !this.password) {
        this.error = 'Please enter your ' + this.clockMethod + '.';
        return;
      }
    } else if (!this.password) {
      this.error = 'Please enter your ' + this.clockMethod + '.';
      return;
    }

    if (this.changePassword && !this.dbHelper.Staff_CheckPassword(this.selectedStaff, this.password)) {
      this.error = 'Please enter valid ' + this.clockMethod;
      return;
    }

    const finalPassword = this.newPassword ? this.newPassword : this.password;

    if (!this.selectedStaff.shaPassword) {
      this.selectedStaff.ClockPassword = finalPassword;
      // Save shaPassword
      this.selectedStaff.shaPassword = this.dbHelper.Staff_GetSHAPassword(finalPassword);
    }

    if (!this.changePassword && !this.dbHelper.Staff_CheckPassword(this.selectedStaff, finalPassword)) {
      this.error = 'Please enter valid ' + this.clockMethod;
    } else {
      this.selectedStaff.ClockPassword = finalPassword;
      // Save shaPassword
      this.selectedStaff.shaPassword = this.dbHelper.Staff_GetSHAPassword(finalPassword);
      if (!this.selectedStaff.CurrentlyClockedIn && this.dbHelper.clockConfig.ShowDeptRolePrompt && (this.dbHelper.clockConfig.UseDeptForClock || this.dbHelper.clockConfig.UseRoleForClock)) {
        if (this.dbHelper.clockConfig.UseDeptForClock) {
          this.selectedDeptForClock = this.selectedStaff.Departments[0];
        }
        this.showDeptRolePrompt = true;
      } else {
        this.clockStaff(this.selectedStaff, this.selectedDeptForClock, this.selectedRoleForClock, false);
      }
    }

  }

  async sendNote() {
    this.note = this.note.trim();
    var description = 'Daily note';
    this.error = '';
    if (this.dbHelper.clockConfig.PromptForNote) {
      if (!this.note) {
        this.error = 'Please enter your note';
        return;
      }

    } else {
      description = 'No lunch break';
      this.note = 'No lunch break was taken by ' + this.selectedStaff.StaffName;
    }

    const body = await this.dbHelper.Time_PostClockTimeNote_Body(this.selectedStaff, description, this.note);
    this.dbHelper.Time_PostClockTimeNote(body).then(val => {
      console.log("Note sent: " + JSON.stringify(val));
    });
    this.showNotesPrompt = false;
    this.noteSent = true;
    this.note = '';
  }

  clearSelectedStaff() {
    this.selectedStaff = null;
    this.showDeptRolePrompt = false;
    this.selectedDeptForClock = '';
    this.selectedRoleForClock = '';
    this.deptSearchTerm = '';
    this.roleSearchTerm = '';
    this.searchTerm = '';
    this.error = '';
    this.changePassword = false;
    this.password = '';
    this.newPassword = '';
    this.confirmPassword = '';
    this.noteSent = false;
    this.note = '';
    this.showNotesPrompt = false;
    this.customStaffListSelectedIndex = -1;

  }

  showMessage(staff, error = false, message = '') {
    if (!error) {
      this.personalisedMessage = "Hello, " + staff.StaffName;
      if (staff.DOB.substring(5) === this.utHelper.dateWithHiphen(new Date()).substring(5)) {
        this.personalisedMessage = "Happy birthday, " + staff.StaffName + "!";
      } else if (staff.CurrentlyClockedIn) {
        const hour = new Date().getHours();
        if (hour >= 0 && hour < 12) this.personalisedMessage = "Good morning, " + staff.StaffName;
        else if (hour >= 12 && hour < 18) this.personalisedMessage = "Good afternoon, " + staff.StaffName;
        else if (hour >= 18 && hour <= 23) this.personalisedMessage = "Good evening, " + staff.StaffName;
      } else if (!staff.CurrentlyClockedIn) {
        this.personalisedMessage = "Goodbye, " + staff.StaffName;
      }
      this.clockingIn = staff.CurrentlyClockedIn;
    } else {
      this.personalisedMessage = message;
      this.clockingIn = false;
    }

    this.hidePersonalisedMessage = false;
    this.animationCtrl.create()
      .addElement(this.messageModal.nativeElement)
      .duration(500)
      .fromTo('opacity', '0', '1')
      .play();
    setTimeout(() => {
      this.animationCtrl.create()
        .addElement(this.messageModal.nativeElement)
        .duration(500)
        .fromTo('opacity', '1', '0')
        .onFinish(() => {
          this.hidePersonalisedMessage = true;
          this.changeDetector.detectChanges();
        })
        .play();

      if (this.forcePIN) {
        this.forcePIN = false;
        this.checkClockingWithFace(5000);
      } else {
        setTimeout(() => {
          this.showCameraFeed();
        }, 5000);
      }
    }, 4000);
  }

  async showSettings(event) {
    const popover = await this.popoverController.create({
      component: SettingsDropdownComponent,
      cssClass: 'settings-popover',
      event: event,
      translucent: true
    });
    return await popover.present();
  }

  async presentDepartmentPicker() {
    var options = [
      {
        text: "All Departments",
        value: "All Departments"
      },
    ];
    this.dbHelper.clockConfig.AvailableDepartments.forEach(dep => {
      options.push({ text: dep, value: dep });
    });
    const picker = await this.pickerController.create({
      animated: true,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Select',
          handler: (val) => {
            this.selectedDepartment = val.options.text;
            if (this.dropDownTimer) clearTimeout(this.dropDownTimer);
            this.dropDownTimer = setTimeout(() => {
              this.selectedDepartment = "All Departments";
            }, 1000 * 60 * 5);
          }
        }
      ],
      columns: [
        {
          name: 'options',
          options: options
        }
      ],
      cssClass: 'custom-picker',
      mode: 'ios',
    });
    picker.columns[0].options.forEach(element => {
      delete element.selected;
      delete element.duration;
      delete element.transform;
    });
    picker.present();
  }

  clockTimes() {
    this.router.navigateByUrl('/login', { state: { nextRoute: '/clock-times', isLoginScreen: true } });
  }

  whosHere() {
    this.printer.isAvailable().then(available => {
      if (available) {
        var toPrint = "Clocked On\n\nLocation: " + this.dbHelper.clockConfig.LocationName + "\nDate: " + this.utHelper.formattedTime(new Date()) + "\n\n";
        this.clockedOnStaff.forEach(staff => {
          toPrint += staff.StaffName + ' ' + (this.dbHelper.clockConfig.ShowLastClockTime && staff.LastClockTimeDisplay ? ('@ ' + staff.LastClockTimeDisplay) : '') + '\n';
        });
        this.printer.print(toPrint, {
          monochrome: true,
          name: "Who's Here"
        }).then(val => {
          console.log(val);
        }).catch(err => {
          this.alertHelper.presentWithoutDismiss("Cannot connect to the printer.", "OK", false, null);
        });
      } else {
        this.alertHelper.presentWithoutDismiss("Printer is not available.", "OK", false, null);
      }
    });
  }

  checkClockingWithFace(timeout = 2000) {
    if (this.dbHelper.clockConfig.ClockOnMethod === 300 || this.dbHelper.clockConfig.ClockOnMethod === 400 || this.dbHelper.clockConfig.ClockOnMethod === 500) {
      this.isClockingWithFace = true;
      setTimeout(() => {
        this.showCameraFeed();
      }, timeout);
    } else {
      this.isClockingWithFace = false;
    }
  }

  showCameraFeed() {
    if (this.showingCameraFeed || this.adminCancelledCameraFeed) return;
    if (this.dbHelper.clockConfig.ClockOnMethod === 400 || this.dbHelper.clockConfig.ClockOnMethod === 500) {
      this.showingCameraFeed = true;
      this.faceRecognitionHelper.login(this.dbHelper.clockConfig.DarkMode ? "true" : "false").then(val => {
        this.showingCameraFeed = false;
        if (val.status === 'REGISTER') {
          this.showFaceRegistrationModal();
          return;
        } else if (val.status === 'PASSWORD') {
          this.showUsePasswordModal();
          return;
        } else if (val.status === 'CANCEL') {
          this.adminCancelledCameraFeed = true;
          return;
        }
        if (val.status === 'SUCCESS' && val.name) {
          const staff = this.dbHelper.getStaffFromStaffCode(val.name);
          staff.usingFace = true;
          if (this.dbHelper.clockConfig.ClockOnMethod === 400) {
            this.showDeptRole(staff);
          } else {
            this.clockStaffTouchless(staff);
          }
        } else {
          this.showMessage(null, true, "Cannot recognise face. Please try again");
        }
      }).catch(err => {
        this.showingCameraFeed = false;
        if (err.status === 'REGISTER') {
          this.showFaceRegistrationModal();
          return;
        } else if (err.status === 'PASSWORD') {
          this.showUsePasswordModal();
          return;
        } else if (err.status === 'CANCEL') {
          this.adminCancelledCameraFeed = true;
          return;
        } else {
          this.showMessage(null, true, "Cannot recognise face. Please try again");
        }
      });
    }
  }

  isRegistered(staff) {
    if (this.dbHelper.clockConfig.ClockOnMethod === 0 || this.dbHelper.clockConfig.ClockOnMethod === 200) return true;
    if (this.isClockingWithFace) {
      if (staff.AlwaysPromptForPWDorPin) {
        return this.dbHelper.Staff_HasPassword(staff);
      } else {
        return this.faceRecognitionHelper.isRegistered(staff.StaffCode);
      }
    } else {
      return this.dbHelper.Staff_HasPassword(staff);
    }
  }

  showFaceRegistrationModal() {
    this.registeringFace = true;
    this.showCustomStaffList = true;
    this.customStaffList = [];
    this.customStaffListSelectedIndex = -1;
    this.dbHelper.staffList.staffList.forEach(element => {
      if (!this.isRegistered(element) && !element.AlwaysPromptForPWDorPin) {
        this.customStaffList.push(element);
      }
    });
  }

  showUsePasswordModal() {
    this.showCustomStaffList = true;
    this.customStaffList = [];
    this.customStaffListSelectedIndex = -1;
    this.dbHelper.staffList.staffList.forEach(element => {
      if (element.AlwaysPromptForPWDorPin) {
        this.customStaffList.push(element);
      }
    });
  }

  get currentDateAndTime() {
    var d = new Date();
    var s = this.utHelper.convertDayToString(d.getDay(), true) + ', ';
    s += d.getDate() + ' ';
    s += this.utHelper.convertMonthToString(d.getMonth(), true) + ', ';
    s += this.utHelper.formatAMPM(d);
    return s;
  }

  get clockedOnStaff() {
    const arr = [];
    if (this.dbHelper.staffList && this.dbHelper.staffList.staffList) {
      this.dbHelper.staffList.staffList.forEach(staff => {
        if (staff.CurrentlyClockedIn) {
          if (this.selectedDepartment === "All Departments" || staff.Departments.includes(this.selectedDepartment)) {
            if (this.searchTerm === '' || staff.StaffName.toLowerCase().startsWith(this.searchTerm.toLowerCase()) || staff.Firstname.toLowerCase().startsWith(this.searchTerm.toLowerCase()) || staff.Surname.toLowerCase().startsWith(this.searchTerm.toLowerCase())) {
              arr.push(staff);
            }
          }
        }
      });
    }
    return arr;
  }

  get clockedOffStaff() {
    const arr = [];
    if (this.dbHelper.staffList && this.dbHelper.staffList.staffList) {
      this.dbHelper.staffList.staffList.forEach(staff => {
        if (!staff.CurrentlyClockedIn) {
          if (this.selectedDepartment === "All Departments" || staff.Departments.includes(this.selectedDepartment)) {
            if (this.searchTerm === '' || staff.StaffName.toLowerCase().startsWith(this.searchTerm.toLowerCase()) || staff.Firstname.toLowerCase().startsWith(this.searchTerm.toLowerCase()) || staff.Surname.toLowerCase().startsWith(this.searchTerm.toLowerCase())) {
              arr.push(staff);
            }
          }
        }
      });
    }
    return arr;
  }

  roles() {
    var arr = [];
    arr = this.dbHelper.clockConfig.AvailableRoles;
    if (this.dbHelper.clockConfig.UseLinkedRoles && this.selectedDeptForClock) {
      if (Object.keys(this.dbHelper.clockConfig.LinkedRoles).includes(this.selectedDeptForClock)) {
        arr = this.dbHelper.clockConfig.LinkedRoles[this.selectedDeptForClock];
      } else {
        arr = this.dbHelper.clockConfig.AvailableRoles;
      }
      if (!arr.includes(this.selectedRoleForClock)) {
        this.selectedRoleForClock = '';
      }
    }
    arr.sort();

    // Search
    if (this.roleSearchTerm.trim()) {
      arr = arr.filter(item => item.toLowerCase().startsWith(this.roleSearchTerm.trim().toLowerCase()));
    }

    if (!arr.includes(this.selectedRoleForClock)) {
      this.selectedRoleForClock = '';
    }

    return arr;
  }

  departments() {
    var arr = [];
    arr = this.selectedStaff.Departments;
    if (this.dbHelper.clockConfig.UseLinkedRoles && this.selectedRoleForClock) {
      arr = [];
      Object.keys(this.dbHelper.clockConfig.LinkedRoles).forEach(key => {
        if (this.selectedStaff.Departments.includes(key) && this.dbHelper.clockConfig.LinkedRoles[key].includes(this.selectedRoleForClock)) {
          arr.push(key);
        }
      });
      this.selectedStaff.Departments.forEach(el => {
        if (!Object.keys(this.dbHelper.clockConfig.LinkedRoles).includes(el) && !arr.includes(el)) {
          arr.push(el);
        }
      });
      if (arr.length === 0) {
        arr = this.selectedStaff.Departments;
      }
      if (!arr.includes(this.selectedDeptForClock)) {
        this.selectedDeptForClock = '';
      }
    }
    arr.sort();

    // Search
    if (this.deptSearchTerm.trim()) {
      arr = arr.filter(item => item.toLowerCase().startsWith(this.deptSearchTerm.trim().toLowerCase()));
    }

    if (!arr.includes(this.selectedDeptForClock)) {
      this.selectedDeptForClock = '';
    }

    return arr;
  }

}
