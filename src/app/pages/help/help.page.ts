import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit {

  city = 'Adelaide';
  phone = '(08) 7123 2993';
  phoneTel = '0871232993';

  city2 = '';
  phone2 = '';
  phoneTel2 = '';

  opStr = '';
  ccct = '';

  interval;

  constructor(private navController: NavController, private changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
    this.getTZ();
    this.setOpenClosing();
    this.interval = setInterval(() => {
      this.getTZ();
      this.setOpenClosing();
      this.changeDetector.markForCheck();
    }, 60000);
  }

  ionViewWillLeave() {
    clearInterval(this.interval);
  }

  timeDiff(earlierDate, futureDate) {

    var calcNewYear = '';
    var date_future = futureDate;
    var date_now = earlierDate;

    var seconds = Math.floor((date_future - (date_now)) / 1000);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);

    hours = hours - (days * 24);
    minutes = minutes - (days * 24 * 60) - (hours * 60);
    seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

    //$("#time").text("Time until new year:\nDays: " + days + " Hours: " + hours + " Minutes: " + minutes + " Seconds: " + seconds);
    var js = { "Days": days, "Hours": hours, "Minutes": minutes };
    return js//JSON.parse(js);
  }

  setOpenClosing() {
    var acstTimestr = new Date().toLocaleString("en-US", { timeZone: "Australia/Adelaide" });
    var yacstTimestr = new Date().toLocaleString("en-AU", { timeZone: "Australia/Adelaide" });
    var acstTime = new Date(acstTimestr);
    var currentTimeStr = new Date().toLocaleString("en-US");
    var ycurrentTimeStr = new Date().toLocaleString("en-AU");
    var currTime = new Date(currentTimeStr);
    this.ccct = "Your time is " + ycurrentTimeStr + "<br>Current helpdesk time is " + yacstTimestr;
    var dy = acstTime.getFullYear();
    var dmm = acstTime.getMonth(); // zero based so we need to add one
    var dd = acstTime.getDate();
    var dn = acstTime.getDay();
    var dh = acstTime.getHours();
    var dm = acstTime.getMinutes();

    var hdopentime = new Date(dy, dmm, dd, 8, 0);
    var hdclosetime = new Date(dy, dmm, dd, 18, 0);

    var tDiffopen = this.timeDiff(acstTime, hdopentime);
    var tDiffclose = this.timeDiff(acstTime, hdclosetime);
    ;
    var cdn = currTime.getDay();
    var cdh = currTime.getHours();
    var cdm = currTime.getMinutes();

    this.opStr = '';
    if (dn === 0 || dn === 6 || (dn === 5 && dh >= 18)) {
      this.opStr = 'The helpdesk will re-open on Monday 8:00am ACST (except for national public holidays)';
      return true;
    }
    if (dh < 8) {
      let hourdif = tDiffopen.Hours;// (8 - dh);
      let mindif = tDiffopen.Minutes;//(60 - dm);
      this.opStr = 'The helpdesk will re-open in ';
      if (hourdif > 0) {
        this.opStr = this.opStr + hourdif.toString() + " hour(s) and ";
      }
      this.opStr = this.opStr + mindif.toString() + ' mins ';
      return true;
    }
    if (dh >= 18) {
      let tom = new Date(acstTime.getFullYear(), acstTime.getMonth(), acstTime.getDate() + 1);
      this.opStr = 'The helpdesk will re-open tomorrow at 8:00 am ACST (except for national public holidays)';
      return true;
    }
    if (dh >= 8 && dh < 18) {
      var ctt = '';
      let vc = tDiffclose.Hours; //(18 - dh);
      let vmc = tDiffclose.Minutes;//(60 - dm);
      if (vc !== 0) {
        ctt = vc.toString() + " hour(s) and "
      }
      ctt = ctt + vmc.toString() + ' mins ';
      this.opStr = 'The helpdesk will be closing in ' + ctt;
      return true;
    }
    return false;
  }

  getTZ() {
    const tzid = Intl.DateTimeFormat().resolvedOptions().timeZone;
    //alert(tzid);
    // Australia/Adelaide
    // Australia/Darwin
    // Australia/Brisbane
    // Australia/Sydney covers melb and act too
    // Australia/Hobart
    // Australia/Perth
    switch (tzid) {
      case "Australia/Darwin":
        break;
      case "Australia/Brisbane":
        this.city = 'Brisbane';
        this.phone = '(07) 3056 0287';
        this.phoneTel = '0730560287';
        break;
      case "Australia/Sydney":
        this.city = 'Sydney';
        this.phone = '(02) 8188 7516';
        this.phoneTel = '0281887516';

        this.city2 = 'Melbourne';
        this.phone2 = '(03) 9008 6359';
        this.phoneTel2 = '0390086359';
        break;
      case "Australia/Hobart":
        this.city = 'Hobart';
        this.phone = '(03) 6108 2116';
        this.phoneTel = '0361082116';
        break;
      case "Australia/Perth":
        this.city = 'Perth';
        this.phone = '(08) 6365 5680';
        this.phoneTel = '0863655680';
        break;
      default:
        break;
    }
    console.log(tzid);
  }

  get state() {
    // state is (OPEN or CLOSED)
    // if its between 9am - 6pm Mon - Friday
    var acstTimestr = new Date().toLocaleString("en-US", { timeZone: "Australia/Adelaide" });
    var acstTime = new Date(acstTimestr);

    var dn = acstTime.getDay();
    var dh = acstTime.getHours();
    var dm = acstTime.getMinutes();

    if (dn === 0 || dn === 6) return 'closed';
    if (dh < 8 || dh >= 18) return 'closed';
    return 'open';

  }

  close() {
    this.navController.back();
  }

}
