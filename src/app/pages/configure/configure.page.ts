import { Component, OnInit } from '@angular/core';
import { LoadingHelper } from 'src/app/helpers/LoadingHelper';
import { UserPreferencesHelper } from 'src/app/helpers/UserPreferencesHelper';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { NavController, PickerController } from '@ionic/angular';
import { DatabaseHelper } from 'src/app/helpers/DatabaseHelper';
import { ConstantsHelper } from 'src/app/helpers/ConstantsHelper';
import { AuthHelper } from 'src/app/helpers/AuthHelper';

@Component({
  selector: 'app-configure',
  templateUrl: './configure.page.html',
  styleUrls: ['./configure.page.scss'],
})
export class ConfigurePage implements OnInit {

  error = "";

  newLocation = "";

  dropDownLists;

  constructor(public dbHelper: DatabaseHelper, private loadingHelper: LoadingHelper, private router: Router, private navCtrl: NavController, private pickerController: PickerController, private userPreferencesHelper: UserPreferencesHelper, private authHelper: AuthHelper) {
  }

  ngOnInit() {
    this.loadingHelper.present();
    this.dbHelper.TimeSetup_PopulateDropdowns().then(val => {
      console.log(val);

      if (val.ResultStr === "OK") {
        this.dropDownLists = val;
        this.configureSelectedOptions();
        this.loadingHelper.dismiss();
      }
    });
  }

  configureSelectedOptions() {

    // Configure Site
    var selected = false;
    this.dropDownLists.Sites.forEach(element => {
      if (element.Selected) {
        this.dbHelper.clockConfig.SiteName = element.Text;
        this.dbHelper.clockConfig.SiteCode = element.Value;
        selected = true;
      }
    });
    if (!selected) {
      this.dbHelper.clockConfig.SiteName = this.dropDownLists.Sites[0].Text;
      this.dbHelper.clockConfig.SiteCode = this.dropDownLists.Sites[0].Value;
    }

    // Configure Location
    this.dropDownLists.ClockingLocations.forEach(element => {
      if (element.SiteCode === this.dbHelper.clockConfig.SiteCode) {
        if (element.SelectedLocGUID) {
          this.dbHelper.clockConfig.ClockPointID = element.SelectedLocGUID;
          element.Items.forEach(loc => {
            if (loc.LocGUID === element.SelectedLocGUID) {
              this.dbHelper.clockConfig.LocationName = loc.LocDescription;
              this.dbHelper.clockConfig.ShowSurnamefirst = loc.ShowSurnamefirst;
              this.dbHelper.clockConfig.ShowLastClockTime = loc.ShowLastClockTime;
            }
          });
        } else {
          this.dbHelper.clockConfig.ClockPointID = element.Items[0].LocGUID;
          this.dbHelper.clockConfig.LocationName = element.Items[0].LocDescription;
          this.dbHelper.clockConfig.ShowSurnamefirst = element.Items[0].ShowSurnamefirst;
          this.dbHelper.clockConfig.ShowLastClockTime = element.Items[0].ShowLastClockTime;
        }
      }
    });

  }

  async presentSitePicker() {
    var options = [];

    this.dropDownLists.Sites.forEach(element => {

      options.push({
        text: element.Text,
        value: element.Value,

      });
    });

    const picker = await this.pickerController.create({
      animated: true,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Select',
          handler: (val) => {
            this.dbHelper.clockConfig.SiteName = val.options.text;
            this.dbHelper.clockConfig.SiteCode = val.options.value;
            this.configureSelectedOptions();
          }
        }
      ],
      columns: [
        {
          name: 'options',
          options: options
        }
      ],
      cssClass: 'custom-picker',
      mode: 'ios',
    });
    picker.columns[0].options.forEach(element => {
      delete element.selected;
      delete element.duration;
      delete element.transform;
    });
    picker.present();
  }

  async presentLocationPicker() {
    var options = [];
    this.dropDownLists.ClockingLocations.forEach(element => {
      if (element.SiteCode === this.dbHelper.clockConfig.SiteCode) {
        element.Items.forEach(loc => {
          options.push({
            text: loc.LocDescription,
            value: loc.LocGUID,
            ShowSurnamefirst: loc.ShowSurnamefirst,
            ShowLastClockTime: loc.ShowLastClockTime
          });
        });
      }
    });

    const picker = await this.pickerController.create({
      animated: true,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Select',
          handler: (val) => {
            this.dbHelper.clockConfig.LocationName = val.options.text;
            this.dbHelper.clockConfig.ClockPointID = val.options.value;
            options.forEach(el => {
              if (el.value === val.value) {
                this.dbHelper.clockConfig.ShowSurnamefirst = el.ShowSurnamefirst;
                this.dbHelper.clockConfig.ShowLastClockTime = el.ShowLastClockTime;
              }
            });
          }
        }
      ],
      columns: [
        {
          name: 'options',
          options: options
        }
      ],
      cssClass: 'custom-picker',
      mode: 'ios',
    });
    picker.columns[0].options.forEach(element => {
      delete element.selected;
      delete element.duration;
      delete element.transform;
    });
    picker.present();
  }

  async presentClockMethodPicker() {

    const picker = await this.pickerController.create({
      animated: true,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Select',
          handler: (val) => {
            this.dbHelper.clockConfig.ClockOnMethod = val.options.value;
          }
        }
      ],
      columns: [
        {
          name: 'options',
          options: ConstantsHelper.ClockMethods
        }
      ],
      cssClass: ['custom-picker', 'wide-picker'],
      mode: 'ios',
    });
    picker.columns[0].options.forEach(element => {
      delete element.selected;
      delete element.duration;
      delete element.transform;
    });
    picker.present();
  }

  configure() {
    this.loadingHelper.present();
    this.newLocation = this.newLocation.trim();
    if (this.newLocation) {
      this.dbHelper.clockConfig.ClockPointID = "";
    }
    this.dbHelper.TimeSetup_Save(this.newLocation).then(val => {
      this.loadingHelper.dismiss();
      if (val.ResultStr === "OK") {
        this.dbHelper.clockConfig = val.resConfig;
        this.dbHelper.saveTokens(val);
        this.dbHelper.saveClockConfigLocally();

        this.authHelper.stationConfigured(true);

        this.dbHelper.staffList = null;

        this.router.navigateByUrl("/");
        
      } else {
        this.error = val.ResultStr;
      }
      
    }).catch(err => {
      this.loadingHelper.dismiss();
      console.log(err);
      this.error = "Something went wrong while saving, please try again."
    });
  }

  cancel() {
    this.navCtrl.back();
  }

  get selectedClockMethod() {
    for (const el of ConstantsHelper.ClockMethods) {
      if (el.value === this.dbHelper.clockConfig.ClockOnMethod) {
        return el.text;
      }
    }
  }

}
