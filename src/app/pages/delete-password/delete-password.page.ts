import { Component, OnInit } from '@angular/core';
import { PickerController } from '@ionic/angular';
import { DatabaseHelper } from 'src/app/helpers/DatabaseHelper';
import { Router } from '@angular/router';
import { LoadingHelper } from 'src/app/helpers/LoadingHelper';
import { AlertHelper } from 'src/app/helpers/AlertHelper';
import { UserPreferencesHelper } from 'src/app/helpers/UserPreferencesHelper';
import { FaceRecognitionHelper } from 'src/app/helpers/FaceRecognitionHelper';

@Component({
  selector: 'app-delete-password',
  templateUrl: './delete-password.page.html',
  styleUrls: ['./delete-password.page.scss'],
})
export class DeletePasswordPage implements OnInit {

  error = '';

  selectedStaff = 'Select an employee';
  selectedStaffCode = '';
  selectedStaffObj;

  staffList;

  constructor(private pickerController: PickerController, public dbHelper: DatabaseHelper, private router: Router, private loadingHelper: LoadingHelper, private alertHelper: AlertHelper, private userPreferencesHelper: UserPreferencesHelper, private faceRecognitionHelper: FaceRecognitionHelper) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
      this.loadStaffList(true);
  }

  loadStaffList(showLoading = true): Promise<any> {
    this.error = '';
    if (showLoading) this.loadingHelper.present();
    return new Promise(res => {
      this.dbHelper.Time_GetUnfilteredStaffList().then(val => {
        console.log(val);
        if (val.ResultStr == 'OK') {
          this.staffList = val.staffList;
        }
        this.loadingHelper.dismiss();
      }).finally(() => res());
    });
    
  }

  async presentStaffPicker() {
    var options = [];
    this.staffList.forEach(element => {
      if (this.dbHelper.clockConfig.ClockOnMethod === 300 || this.dbHelper.clockConfig.ClockOnMethod === 400 || this.dbHelper.clockConfig.ClockOnMethod === 500) {
        if (this.faceRecognitionHelper.isRegistered(element.StaffCode) || (element.AlwaysPromptForPWDorPin && this.dbHelper.Staff_HasPassword(element))) {
          options.push({
            text: element.StaffName,
            value: element.StaffCode
          });
        }
      } else if (this.dbHelper.Staff_HasPassword(element)) {
        options.push({
          text: element.StaffName,
          value: element.StaffCode
        });
      }
    });

    const picker = await this.pickerController.create({
      animated: true,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Select',
          handler: (val) => {
            this.selectedStaff = val.options.text;
            this.selectedStaffCode = val.options.value;
            this.selectedStaffObj = this.dbHelper.getStaffFromStaffCode(this.selectedStaffCode);
            this.error = '';
          }
        }
      ],
      columns: [
        {
          name: 'options',
          options: options
        }
      ],
      cssClass: 'custom-picker',
      mode: 'ios',
    });
    picker.columns[0].options.forEach(element => {
      delete element.selected;
      delete element.duration;
      delete element.transform;
    });
    picker.present();
  }

  cancel() {
    this.router.navigateByUrl('/');
  }

  reset() {
    this.error = '';
    if (!this.selectedStaffCode) {
      this.error = "Please select an employee."
      return;
    }
    if (!this.selectedStaffObj.AlwaysPromptForPWDorPin && (this.dbHelper.clockConfig.ClockOnMethod === 300 || this.dbHelper.clockConfig.ClockOnMethod === 400 || this.dbHelper.clockConfig.ClockOnMethod === 500)) {
      this.faceRecognitionHelper.clear(this.selectedStaffCode);
      this.alertHelper.presentWithoutDismiss(this.selectedStaff + "'s face has been removed.", "OK", false, null);
      this.selectedStaff = 'Select an employee';
      this.selectedStaffCode = '';
    } else {
      this.loadingHelper.present();
      this.dbHelper.Staff_ResetPassword(this.selectedStaffCode).then(val => {
        this.dbHelper.staffList.staffList.find((o, i) => {
          if (o.StaffCode === this.selectedStaffCode) {
            this.dbHelper.staffList.staffList[i].ClockPassword = '';
            this.dbHelper.staffList.staffList[i].shaPassword = '';
            this.userPreferencesHelper.set(UserPreferencesHelper.STAFF_LIST, this.dbHelper.staffList);
            return true;
          }
        });
        this.selectedStaffObj.ClockPassword = '';
        this.selectedStaffObj.shaPassword = '';
        this.selectedStaffCode = '';
        this.loadStaffList(false).then(() => {
          this.alertHelper.presentWithoutDismiss(this.selectedStaff + "'s PIN/password has been reset.", "OK", false, () => this.selectedStaff = 'Select an employee');
        });
      });
    }
  }

}
