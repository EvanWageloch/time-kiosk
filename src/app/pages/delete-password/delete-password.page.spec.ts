import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeletePasswordPage } from './delete-password.page';

describe('DeletePasswordPage', () => {
  let component: DeletePasswordPage;
  let fixture: ComponentFixture<DeletePasswordPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletePasswordPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeletePasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
