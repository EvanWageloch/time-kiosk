import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatabaseHelper } from 'src/app/helpers/DatabaseHelper';
import { LoadingHelper } from 'src/app/helpers/LoadingHelper';
import { AlertHelper } from 'src/app/helpers/AlertHelper';
import { NavController, PickerController } from '@ionic/angular';
import { UserPreferencesHelper } from 'src/app/helpers/UserPreferencesHelper';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginOption = this.dbHelper.clockConfig.PureAPI ? "pureApi" : "legacyApi";

  // email = "";
  // password = "";

  email = "dnwageloch@gmail.com";
  password = "Dnwageloch2020!";


  username = 'Select username';

  error = "";

  nextRoute = '';
  disableCancel = false;
  isLoginScreen = false;

  usernames = [];

  constructor(private route: ActivatedRoute, private router: Router, public dbHelper: DatabaseHelper, public loadingHelper: LoadingHelper, public alertHelper: AlertHelper, private navCtrl: NavController, private userPreferencesHelper: UserPreferencesHelper, private pickerController: PickerController) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.nextRoute = this.router.getCurrentNavigation().extras.state.nextRoute;
        this.disableCancel = this.router.getCurrentNavigation().extras.state.disableCancel === true;
        this.isLoginScreen = this.router.getCurrentNavigation().extras.state.isLoginScreen === true;
      }
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    // this.clearData();
    if (this.dbHelper.clockConfig.Initialised && !this.dbHelper.clockConfig.PureAPI) {
      this.loadingHelper.present();
      this.dbHelper.Timesetup_GetCloudUsers().then(val => {
        console.log(val);
        val.Users.forEach(element => {
          this.usernames.push(element.Text);
        });
        this.username = this.usernames[0];
        this.loadingHelper.dismiss();
      });
    }
    if (!this.dbHelper.clockConfig.Initialised) {
      this.disableCancel = true;
    }
  }

  clearData() {
    this.password = "";
    this.email = "";
    this.username = "";
    this.error = "";
    this.disableCancel = true;
  }

  login() {

    this.error = "";

    this.dbHelper.clockConfig.PureAPI = this.loginOption === "pureApi";

    if (!this.dbHelper.clockConfig.PureAPI && this.dbHelper.clockConfig.Initialised) {
      this.email = this.username;
    }

    this.email = this.email.trim();
    this.password = this.password.trim();

    if (!this.email) {
      this.error = "Please enter " + (this.loginOption == "pureApi" ? "email" : "Site Code");
      return;
    }
    if (!this.password) {
      this.error = "Please enter " + (this.loginOption == "pureApi" ? "password" : "Install Code");
      return;
    }

    if (!this.dbHelper.clockConfig.PureAPI && !this.dbHelper.clockConfig.Initialised) {
      this.dbHelper.clockConfig.SiteCode = this.email;
    }

    this.loadingHelper.present();

    this.dbHelper.Timesetup_Dologin(this.email, this.password).then(async val => {
      this.loadingHelper.dismiss()
      console.log(val);
      if (val === "OK") {
        await this.userPreferencesHelper.set("auth", { email: this.email, password: this.password });
        this.router.navigateByUrl(this.nextRoute);
      } else if (val !== undefined) {
        this.error = "Incorrect email/username or password.";
      }
    }).catch(err => {
      this.loadingHelper.dismiss()
      this.error = "Incorrect email/username or password.";
    });
  }

  reset() {
    this.error = "";
    this.alertHelper.present("Are you sure you want to reset this workstation?", "Yes", "No", true, () => {
      this.loadingHelper.present();
      this.dbHelper.clockConfig.PureAPI = this.loginOption === "pureApi";
      if (!this.dbHelper.clockConfig.PureAPI && this.dbHelper.clockConfig.Initialised) {
        this.email = this.username;
      }
      this.dbHelper.Timesetup_Dologin(this.email, this.password).then(val => {
        this.loadingHelper.dismiss()
        console.log(val);
        if (val === "OK") {
          this.dbHelper.TimeSetup_reset();
          this.clearData();
        } else {
          this.error = "Incorrect email/username or password.";
        }
      }).catch(err => {
        this.loadingHelper.dismiss()
        this.error = "Incorrect email/username or password.";
      });
    }, () => {

    });
  }

  cancel() {
    if (!this.disableCancel) {
      this.navCtrl.back();
    }
  }

  goTo(url) {
    this.router.navigateByUrl(url);
  }

  async presentUsernamePicker() {
    var options = [];

    this.usernames.forEach(u => {
      options.push({
        text: u,
        value: u
      });
    });

    const picker = await this.pickerController.create({
      animated: true,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Select',
          handler: (val) => {
            this.username = val.options.text;
          }
        }
      ],
      columns: [
        {
          name: 'options',
          options: options
        }
      ],
      cssClass: 'custom-picker',
      mode: 'ios',
    });
    picker.columns[0].options.forEach(element => {
      delete element.selected;
      delete element.duration;
      delete element.transform;
    });
    picker.present();
  }

}
