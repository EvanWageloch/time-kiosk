import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { DatabaseHelper } from 'src/app/helpers/DatabaseHelper';
import { ConstantsHelper } from 'src/app/helpers/ConstantsHelper';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  version = '';
  spaceAvailable = '0 MB';

  constructor(private navController: NavController, public dbHelper: DatabaseHelper, private appVersion: AppVersion) { }

  ngOnInit() {
    this.appVersion.getVersionNumber().then(val => {
      this.version = val;
    });
    this.dbHelper.getAvailableSpace().then(val => {
      var diskSizeInMB = val / 1024 / 1024;
      this.spaceAvailable = diskSizeInMB.toFixed(2) + ' MB';
      if (diskSizeInMB > 1024) {
        var diskSizeInGB = diskSizeInMB / 1024;
        this.spaceAvailable = diskSizeInGB.toFixed(2) + ' GB';
      }
    });
  }

  get clockMethod() {
    return ConstantsHelper.ClockMethods.find(el => el.value === this.dbHelper.clockConfig.ClockOnMethod).text;
  }

  close() {
    this.navController.back();
  }

}
