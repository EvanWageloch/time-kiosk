export class ClockConfig {

    ResultStr = "ERROR";
    SiteCode = "";
    SiteName = "";
    SetupBy = "";
    RefreshToken = "";
    Token = "";
    AvailableDepartments = [];
    AvailableRoles = [];
    ShowDeptFilter = false;
    UseDeptForClock = false;
    UseRoleForClock = false;
    ShowDeptRolePrompt = false;
    WhenToshowPrompt = "IN";
    UseLinkedRoles = false;
    LinkedRoles = [[]];
    BusinessHours = null;
    AssumeClockOffAfterHour = 14;
    locationId = "-1";
    LocationName = "";
    ClockPointID = "-1";
    PureAPI = true;
    Initialised = false;
    ClockOnMethod = 0;
    RestartTime = "";
    IsApp = true;
    ShowSurnamefirst = false;
    EmailLateClockin = false;
    EmailAddresses = "";
    LateClockinMins = 0;
    ShowLastClockTime = true;
    StaffRefreshIntervalMinutes = 5;
    ConfigRefreshIntervalMinutes = 120;
    UsingRebrand = false;
    DarkMode = false;
    ColourBlindAssist = false;
    DeptFilterAndSearchboxesTimeoutSeconds = 30;
    NotesButtonText;
    ShowNotesButtonOnRoleScreen;
    PromptForNote;
    FireBaseId = "";
    ShowEmpSearch = false;

    constructor() {
        
    }
}