import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-settings-dropdown',
  templateUrl: './settings-dropdown.component.html',
  styleUrls: ['./settings-dropdown.component.scss'],
})
export class SettingsDropdownComponent implements OnInit {

  constructor(private router: Router, private popoverController: PopoverController) { }

  ngOnInit() { }

  goTo(url: string) {
    this.popoverController.dismiss();
    if (url === 'configure') {
      this.router.navigateByUrl('/login', { state: { nextRoute: '/configure' } });
    } else if (url === 'delete-password') {
      this.router.navigateByUrl('/login', { state: { nextRoute: '/delete-password', isLoginScreen: true } });
    } else {
      this.router.navigateByUrl(url);
    }
  }

}
