import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Device } from '@ionic-native/device/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Printer } from '@ionic-native/printer/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { File } from '@ionic-native/file/ngx';
import { Insomnia } from '@ionic-native/insomnia/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { DatabaseHelper } from './helpers/DatabaseHelper';
import { AlertHelper } from './helpers/AlertHelper';
import { AuthHelper } from './helpers/AuthHelper';
import { LoadingHelper } from './helpers/LoadingHelper';
import { UserPreferencesHelper } from './helpers/UserPreferencesHelper';

import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { ConstantsHelper } from './helpers/ConstantsHelper';
import { SettingsDropdownComponent } from './components/settings-dropdown/settings-dropdown.component';
import { UtilityHelper } from './helpers/UtilityHelper';
import { Luxand } from 'resources/wrappers/@ionic-native/luxand/ngx';
import { FaceRecognitionHelper } from './helpers/FaceRecognitionHelper';


@NgModule({
  declarations: [AppComponent, LoadingSpinnerComponent, SettingsDropdownComponent],
  entryComponents: [LoadingSpinnerComponent, SettingsDropdownComponent],
  imports: [
    IonicModule.forRoot({ mode: "ios", rippleEffect: false, swipeBackEnabled: false }),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
  ],
  providers: [

    DatabaseHelper,
    AlertHelper,
    AuthHelper,
    LoadingHelper,
    UserPreferencesHelper,
    ConstantsHelper,
    UtilityHelper,
    FaceRecognitionHelper,

    StatusBar,
    SplashScreen,
    ScreenOrientation,
    Network,
    Device,
    Printer,
    AppVersion,
    File,
    Insomnia,
    Luxand,

    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
