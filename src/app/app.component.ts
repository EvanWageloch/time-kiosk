import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DatabaseHelper } from './helpers/DatabaseHelper';
import { UserPreferencesHelper } from './helpers/UserPreferencesHelper';
import { AuthHelper } from './helpers/AuthHelper';
import { Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { FaceRecognitionHelper } from './helpers/FaceRecognitionHelper';
import { Insomnia } from '@ionic-native/insomnia/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private databaseHelper: DatabaseHelper,
    private userPreferencesHelper: UserPreferencesHelper,
    private faceRecognitionHelper: FaceRecognitionHelper,
    private authHelper: AuthHelper,
    private router: Router,
    private screenOrientation: ScreenOrientation,
    private insomnia: Insomnia
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#d3007a');
      this.statusBar.hide();
      // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
      this.insomnia.keepAwake();

      this.databaseHelper.setupIsOnline();

      // Wait for secure storage to initialise
      await this.userPreferencesHelper.createStorage();
      await this.faceRecognitionHelper.initialise();
      // Check station configured
      await this.authHelper.checkConfigState();
      this.authHelper.checkUserLoggedIn();

      // Load saved user data
      if (this.authHelper.configState.value) {
        await this.databaseHelper.loadSavedData();
        this.router.navigateByUrl('/');
        // this.router.navigateByUrl('/login', { state: { nextRoute: '/clock-times', isLoginScreen: true } });
        // this.router.navigateByUrl('/login', { state: { nextRoute: "/configure" } });
      } else {
        this.router.navigateByUrl('/login', { state: { nextRoute: "/configure", disableCancel: true } });
      }

      setTimeout(() => {
        this.splashScreen.hide();
      }, 300);
    });
  }
}
